MODULE constants

    USE, INTRINSIC::iso_fortran_env
    IMPLICIT NONE

    REAL(KIND = 8), parameter :: pi = 3.141592653589793238462643383279502884197_8
    REAL(KIND = 8), parameter :: kB = 0.6950387_8 !cm-1 / K
    REAL(KIND = 8), parameter :: hbar = 5.30886139629910E-12_8 !cm-1 . s
    REAL(KIND = 8), parameter :: light = 299792458.0_8 !m / s
    REAL(KIND = 8), parameter :: muB = 0.466866577042538_8 !cm-1 / T
    INTEGER, parameter        :: mode_limit = 10000


END MODULE constants
