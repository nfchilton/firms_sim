!
!                                      ┌─┐┬┬─┐┌┬┐┌─┐    ┌─┐┬┌┬┐
!                                      ├┤ │├┬┘│││└─┐    └─┐││││
!                                      └  ┴┴└─┴ ┴└─┘────└─┘┴┴ ┴
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
!                                                                                                  !
!   firms_sim.f90                                                                                  !
!                                                                                                  !
!   This program is a part of firms_sim                                                            !
!                                                                                                  !
!                                                                                                  !
!                                                                                                  !
!                                                                                                  !
!      Authors:                                                                                    !
!       Jon Kragskow                                                                               !
!       Nicholas Chilton                                                                           !
!                                                                                                  !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

PROGRAM firms_sim
    use matrix_tools
    IMPLICIT NONE
    REAL(KIND = 8), ALLOCATABLE       :: mode_energies(:), EQ_H_pho(:,:), field_vecs(:,:,:), &
                                         field_strengths(:), total_absorbance(:,:), &
                                         e_axis(:), CF_vals(:), PHO_vals(:), field_angs(:,:), &
                                         dip_deriv(:,:)
    COMPLEX(KIND = 8), ALLOCATABLE    :: EQ_H_CF(:,:), H_Coup(:,:), EQ_H_total(:,:), &
                                         EQ_H_total_NC(:,:), CF_vecs(:,:)
    REAL(KIND = 8)                    :: gJ, field_lower, field_upper, field_step, lower_e_bound, &
                                         upper_e_bound, OEF(3), temperature, elec_scale, coupling_scale, &
                                         B_quant, fwhm, linewidth
    CHARACTER(LEN = 1000)             :: input_file, polynomials_file, mode_energies_file, CFPs_file, ion, &
                                         single_axis
    INTEGER                           :: elec_dim, num_modes, n_coup_modes, n_vib_states, &
                                         pho_dim, coup_dim, n_field_strengths, n_field_vecs, &
                                         ox_state, zcw_val, resolution, ir_grid_size, j
    INTEGER, ALLOCATABLE              :: mode_numbers(:), basis_labels(:,:)
    CHARACTER(LEN = 500)              :: trans_treat
    LOGICAL                           :: mask_trans, nooef, new_cf_energies

    ! Remove old input file
    CALL EXECUTE_COMMAND_LINE('rm -f firms_sim.out')

    ! Read command line arguments
    CALL PARSE_INPUT_ARGUMENTS(input_file)

    ! Add header to output file
    CALL MAKE_OUTPUT_FILE

    ! Read input file
    CALL READ_INPUT_FILE(input_file, polynomials_file, mode_energies_file, n_coup_modes, num_modes, mode_numbers, &
                         n_vib_states, CFPs_file, field_lower, field_upper, field_step, &
                         zcw_val, single_axis, lower_e_bound, upper_e_bound, resolution, ion, &
                         ox_state, temperature, trans_treat, elec_scale, coupling_scale, &
                         ir_grid_size, mask_trans, nooef, new_cf_energies, fwhm)

    ! Copy input to output
    CALL COPY_INPUT_TO_OUTPUT(input_file)

    ! Calculate quantum numbers, gJ, etc
    CALL ELECTRONIC_OPTIONS(ion, OEF, ox_state, elec_dim, gJ)

    ! Set num_states, read energies, etc
    CALL PHONON_OPTIONS(n_coup_modes, mode_energies, mode_energies_file, num_modes, &
                        n_vib_states, pho_dim, mode_numbers, dip_deriv)

    ! Calculate Gaussian width using FWHM value
    CALL CALC_G_WIDTH(fwhm, linewidth, 'Simulation linewidth')


    ! Set dimensionality of final Hamiltonian
    CALL COUPLING_OPTIONS(pho_dim, elec_dim, coup_dim)

    ! Calculate applied field strengths, field vectors for powder averaging etc
    CALL FIELD_OPTIONS(field_lower, field_upper, field_step, field_vecs, field_angs, &
                       field_strengths, n_field_strengths, n_field_vecs, &
                       zcw_val, single_axis, ir_grid_size)

    ! Calculate equilibrium CF Hamiltonian with Zeeman-z perturbation to quantise KDs
    ! If single axis is selected then quantise along specified field instead
    CALL CALC_EQ_CF_HAMILTONIAN(CFPs_file, elec_dim, EQ_H_CF, CF_vecs, CF_vals, gJ, single_axis, &
                                nooef, OEF, new_cf_energies, B_quant)

    ! Replace CF Hamiltonian with diagonal matrix containing energies
    ! i.e. write CF Hamiltonian in its eigenbasis
    EQ_H_CF = DIAG(CF_vals)

    IF (pho_dim > 1) THEN
        ! Calculate equilibrium Phonon Hamiltonian
        CALL CALC_EQ_PHO_HAMILTONIAN(n_coup_modes, mode_energies, mode_numbers, &
                                     n_vib_states, pho_dim, EQ_H_pho)

        ! Calculate Coupling Hamiltonians
        CALL CALC_COUPLING_HAMILTONIAN(polynomials_file, coup_dim, n_coup_modes, num_modes, &
                                        mode_numbers, elec_dim, n_vib_states, H_Coup, OEF, &
                                        coupling_scale, CF_vecs)
    END IF
    
    ! Generate coupled basis label array
    CALL LABEL_BASIS(elec_dim, pho_dim, n_coup_modes, n_vib_states, coup_dim, basis_labels)

    ! Calculate total Hamiltonian in "zero" field
    ! H_CF includes a small biasing field (see CALC_EQ_CF_HAMILTONIAN)
    CALL CALC_H_EQ_TOTAL(elec_dim, pho_dim, coup_dim, EQ_H_CF, EQ_H_pho, H_Coup, EQ_H_total, &
                         EQ_H_total_NC)

    ! Write zero field information to output file
    CALL WRITE_ZF_INFO(elec_dim, pho_dim, coup_dim, gJ, CF_vecs, CF_vals, EQ_H_pho, EQ_H_total, &
                       EQ_H_total_NC, basis_labels, B_quant)

    DEALLOCATE(EQ_H_total_NC, EQ_H_total)

    ! Calculate Spectrum by averaging over field vectors for each strength
    ! Recalculates field dependent part of H_total and diagonalises
    ! for each strength
    CALL CALC_AV_SPECTRUM(EQ_H_CF, EQ_H_pho, H_Coup, elec_dim, pho_dim, coup_dim, field_vecs, &
                          field_angs, n_field_strengths, n_field_vecs, gJ, lower_e_bound, &
                          upper_e_bound, total_absorbance, e_axis, resolution, temperature, &
                          basis_labels, dip_deriv, trans_treat, elec_scale, ir_grid_size, CF_vecs,&
                          linewidth)

    CALL SAVE_OUTPUT_DATA(field_strengths, total_absorbance, e_axis, field_lower, field_upper, &
                          field_step, mode_numbers, n_coup_modes)

    !IF (n_field_strengths > 1) CALL EXECUTE_COMMAND_LINE('sim_heatmap data.dat')

    CONTAINS

    SUBROUTINE PARSE_INPUT_ARGUMENTS(input_file)
        IMPLICIT NONE
        CHARACTER(LEN=*) :: input_file

        CALL GET_COMMAND_ARGUMENT(1, input_file)
        IF (input_file == '' .OR. input_file == '-h' .OR. input_file == '--h') THEN
            WRITE(6,'(A)') 'Please provide input file as argument'
            WRITE(6,'(A)') 'for example input use -input'
            STOP
        END IF

        IF (input_file == '-input') THEN
            CALL WRITE_EXAMPLE_INPUT
            WRITE(6,'(A)') '~~~~             Blank input written to blank_input.dat             ~~~~'
            WRITE(6,'(A)') '!!!!                  Replace BOLD TEXT as appropriate                  !!!!'
            STOP
        END IF

        IF (input_file == '-subscript') THEN

            OPEN(77, FILE = 'firms_submit.sh', STATUS = 'UNKNOWN')

                WRITE(77,'(A)') '#!/bin/bash --login'
                WRITE(77,'(A)') '#$ -S /bin/bash'
                WRITE(77,'(A)') '#$ -N firms_job # You can change this'
                WRITE(77,'(A)') '#$ -cwd'
                WRITE(77,'(A)') '#$ -pe smp.pe 4 # You can change this - default is 4 cores'
                WRITE(77,'(A)') '#$ -l short # requests short queue up to 12 cores for 1 hour'
                WRITE(77,'(A)') 'export OMP_NUM_THREADS=$NSLOTS # DO NOT CHANGE THIS'
                WRITE(77,'(A)') 'module load compilers/intel/18.0.3'
                WRITE(77,'(A)') 'firms_sim input.dat # Change this if your firms_sim input file is named differently'

            CLOSE(77)

            WRITE(6,'(A)') '~~~~         Submission script written to firms_submit.sh  in CWD         ~~~~'

            STOP        
        END IF
        
    END SUBROUTINE PARSE_INPUT_ARGUMENTS

    SUBROUTINE WRITE_EXAMPLE_INPUT
        IMPLICIT NONE

        OPEN(66, FILE = 'blank_input.dat', STATUS = 'UNKNOWN')

            WRITE(66,'(A)') '****core parameters'
            WRITE(66,'(A)') 'eq cfps file FILENAME'
            WRITE(66,'(A)') 'polynomials file FILENAME'
            WRITE(66,'(A)') 'mode energies file FILENAME'
            WRITE(66,'(A)') 'ion SYMBOL OXSTATE_NUMBER'
            WRITE(66,'(A)') 'modes NUMBER'
            WRITE(66,'(A)') 'use modes NUMBER(S)'
            WRITE(66,'(A)') 'vib states NUMBER'
            WRITE(66,'(A)') 'field LOW HIGH STEP'
            WRITE(66,'(A)') 'energy LOW HIGH NUM_STEPS'
            WRITE(66,'(A)') 'temperature NUMBER'

            WRITE(66,'(A)') '****optional parameters'
            WRITE(66,'(A)') 'zcw NUMBER'
            WRITE(66,'(A)') 'single axis LETTER (x,y,z)'
            WRITE(66,'(A)') 'mask transitions'
            WRITE(66,'(A)') 'nooef'
            WRITE(66,'(A)') 'replace cf energies'
            WRITE(66,'(A)') 'scale electronic NUMBER'
            WRITE(66,'(A)') 'scale coupling NUMBER'
            WRITE(66,'(A)') 'ir grid NUMBER'
            WRITE(66,'(A)') 'fwhm NUMBER'
            WRITE(66,'(A)') 'transition approach SIMPLE/COMPLEX'

        CLOSE(66)

    END SUBROUTINE WRITE_EXAMPLE_INPUT

    SUBROUTINE MAKE_OUTPUT_FILE
        USE version, only : git_version
        Use OMP_LIB
        IMPLICIT NONE
        CHARACTER(LEN = 1000) :: CDUMMY, s
        INTEGER               :: num_threads

        OPEN(66, FILE = 'firms_sim.out', STATUS = 'UNKNOWN', POSITION = 'APPEND')

            WRITE(66,'(A)') '                            ┌─┐┬┬─┐┌┬┐┌─┐    ┌─┐┬┌┬┐                            '
            WRITE(66,'(A)') '                            ├┤ │├┬┘│││└─┐    └─┐││││                            '
            WRITE(66,'(A)') '                            └  ┴┴└─┴ ┴└─┘────└─┘┴┴ ┴                            '
            WRITE(66,'(A)') ''
            WRITE(66,'(A)') '                                  Jon Kragskow                                  '
            WRITE(66,'(A)') '                                Nicholas Chilton                                '
            WRITE(66,'(A)') 

            ! Print git version number    
            WRITE(CDUMMY,'(2A)') 'Version :   ', TRIM(git_version)
            CALL WARNING(CDUMMY, 66)

            ! Print number of threads
!$OMP PARALLEL
    num_threads = omp_get_num_threads()
!$OMP END PARALLEL
            IF (num_threads > 1) THEN
                s = 's'
            ELSE
                s = ''
            END IF

            WRITE(CDUMMY,'(A, I0, 2A)') 'firms_sim is running on ', num_threads, ' thread',TRIM(ADJUSTL(s))
            CALL WARNING(CDUMMY, 66)

#ifdef compilerdebug
            CALL WARNING('COMPILED IN DEBUG MODE',66)
            WRITE(66,*)
#else
            WRITE(66,*)
#endif

        CLOSE(66)


    END SUBROUTINE MAKE_OUTPUT_FILE

    SUBROUTINE COPY_INPUT_TO_OUTPUT(input_file)
        IMPLICIT NONE
        CHARACTER(LEN = 500), INTENT(IN) :: input_file
        CHARACTER(LEN = 500)             :: line
        INTEGER                          :: reason

        ! Open input file
        OPEN(33, FILE = ADJUSTL(TRIM(input_file)), STATUS = 'OLD')

        ! Open output file
        OPEN(66, FILE = "firms_sim.out", STATUS = 'UNKNOWN', POSITION = 'APPEND')

        !Write header in output file
        WRITE(66,'(A)') ''
        CALL SECTION_HEADER('Input File',66)
        WRITE(66,'(A)') ''

        ! Loop through file and copy input to output file
        DO
            READ(33,'(A)',IOSTAT = reason) line

            ! End of file
            IF (reason < 0) THEN
                EXIT
            END IF

            ! Write input to output if line is not blank
            CALL lowercase(line)
            IF (LEN_TRIM(ADJUSTL(line)) /= 0) WRITE(66,'(A)') TRIM(ADJUSTL(line))

        END DO

        ! Close input file
        CLOSE(33)

        ! Close output file
        CLOSE(66)

    END SUBROUTINE COPY_INPUT_TO_OUTPUT

    PURE SUBROUTINE LOWERCASE(str)
        ! Converts str to lowercase characters
        IMPLICIT NONE
        INTEGER                           :: i,gap
        CHARACTER(LEN = *), INTENT(INOUT) :: str
        
        gap = ICHAR('a')-ICHAR('A')
        IF(len(str) > 0) THEN
            DO i=1,len(str)
                IF(str(i:i) <= 'Z') THEN
                    IF(str(i:i) >= 'A') str(i:i) = CHAR(ICHAR(str(i:i)) + gap)
                END IF
            END DO
        END IF

    END SUBROUTINE LOWERCASE

    SUBROUTINE READ_INPUT_FILE(input_file, polynomials_file, mode_energies_file, n_coup_modes, &
                               n_modes, mode_numbers, n_vib_states, CFPs_file, field_lower,&
                               field_upper, field_step, zcw_val, single_axis, &
                               lower_e_bound, upper_e_bound, resolution, ion, ox_state, &
                               temperature, trans_treat, elec_scale, coupling_scale, &
                               ir_grid_size, mask_trans, nooef, new_cf_energies, fwhm)
        USE constants
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(IN)      :: input_file
        CHARACTER(LEN = *), INTENT(OUT)     :: mode_energies_file, polynomials_file, &
                                               CFPs_file, ion, single_axis, &
                                               trans_treat
        CHARACTER(LEN = 1000)               :: line, DUMMY, error_message, p_line
        CHARACTER(LEN = 1000), ALLOCATABLE  :: CDummy_array(:)
        REAL(KIND = 8)                      :: RDUMMY
        REAL(KIND = 8), INTENT(OUT)         :: field_lower, field_upper, field_step, lower_e_bound,&
                                               upper_e_bound, temperature, elec_scale, coupling_scale, fwhm
        INTEGER, INTENT(OUT)                :: n_coup_modes, n_vib_states, ox_state, &
                                               zcw_val, resolution, n_modes, ir_grid_size
        INTEGER, INTENT(OUT), ALLOCATABLE   :: mode_numbers(:)
        INTEGER                             :: i, j, reason, num_entries, IDUMMY1, IDUMMY2
        LOGICAL, INTENT(OUT)                :: mask_trans, nooef, new_cf_energies
        LOGICAL                             :: file_exists
        LOGICAL, ALLOCATABLE                :: modes_logical(:)

        ! Set defaults

        single_axis = ''
        coupling_scale = 1.0_8
        elec_scale = 1.0_8
        mask_trans = .FALSE.
        nooef = .FALSE.
        new_cf_energies = .FALSE.
        polynomials_file = "CFP_polynomials.dat"
        CFPs_file = "EQ_CFPs.dat"
        mode_energies_file = "mode_energies.dat"
        trans_treat = "simple"
        ir_grid_size = 1
        single_axis = "no"
        fwhm = 2.0_8
        zcw_val = 5

        ! Placeholder defaults
        ion = "XX"
        ox_state = 99999
        n_modes = -1
        n_coup_modes = -1
        field_lower = -1
        field_upper = -1
        lower_e_bound  = -1
        upper_e_bound  = -1
        resolution     = -1

        ! Open input file and read data
        OPEN(33, FILE = input_file, STATUS = 'OLD')

        ! Loop through file and read input sections
        DO
            READ(33,'(A)',IOSTAT=reason) line
 
            ! Check file is not corrupted
            IF (reason > 0)  THEN
                CALL PRINT_ERROR('Error reading input file','')
            ! Check if EOF has been reached
            ELSE IF (reason < 0) THEN
                EXIT
            ELSE

                ! Skip blank lines
                IF (LEN_TRIM(line) == 0) CYCLE

                ! Make all of line lowercase
                ! but first keep a copy with the correct case
                ! for filename inputs
                p_line = line
                CALL LOWERCASE(line)

                !  Skip comment lines
                IF (GET_TRIMMED_VAL(line, 1, 1) == '!') CYCLE
                IF (GET_TRIMMED_VAL(line, 1, 1) == '#') CYCLE
                IF (GET_TRIMMED_VAL(line, 1, 1) == '*') CYCLE

                ! Ion type and oxidation state
                IF (GET_TRIMMED_VAL(line, 1, 3) == 'ion') THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 2) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, ion, ox_state

                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error reading Ion',&
                                             'Check input file')
                        END IF
                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in ion specification',&
                                         'Check input file')
                    END IF
                END IF   

                ! Number of modes in molecule/system
                IF (GET_TRIMMED_VAL(line, 1, 5) == 'modes') THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 1) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, n_modes

                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error reading modes',&
                                             'Check input file')
                        END IF           
                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in mode specification',&
                                         'Check input file')
                    END IF
                END IF
        
                ! Modes to consider
                IF (INDEX(TRIM(line),'use modes') /= 0) THEN
                    line = line(10:)

                    ! Replace spaces and commas from line to give numbers and ranges
                    ! e.g. 10  12-15,  30-35 --> 10 12-15 30-35
                    line = replace_space_comma(line)

                    ! Read numbers from line and set modes logical True or False to match
                    num_entries = count_char(TRIM(ADJUSTL(line)), ' ') + 1
                    ALLOCATE(CDummy_array(num_entries))
                    ! Allocate array of True/False to include mode(s) in simulation
                    ALLOCATE(modes_logical(mode_limit))
                    modes_logical = .FALSE.
                    READ(line, *, IOSTAT = reason) CDummy_array

                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error reading remove mode',&
                                             'Check input file')
                        END IF


                    DO i = 1, num_entries

                            ! If a dash is present in the ith element, then remove it and read two numbers
                            IF (count_char(TRIM(ADJUSTL(CDummy_array(i))), '-') == 1) THEN
                                
                                CDummy_array(i) = replace_text(TRIM(ADJUSTL(CDummy_array(i))), '-', ' ', count_char(TRIM(ADJUSTL(line)), '-'))
                                READ(CDummy_array(i), *, IOSTAT = reason) IDUMMY1, IDUMMY2
                            
                                ! Check file is not corrupted or too short
                                IF (reason > 0)  THEN
                                    CALL PRINT_ERROR('Error reading remove mode',&
                                                     'Check input file')
                                END IF

                                ! Set logicals
                                modes_logical(IDUMMY1:IDUMMY2) = .TRUE.

                            ! Else read a single number
                            ELSE
                                READ(CDummy_array(i), *, IOSTAT = reason) IDUMMY1

                                ! Set logical
                                modes_logical(IDUMMY1) = .TRUE.
                            END IF
                    END DO
                    ! Count how many modes have been removed
                    n_coup_modes = COUNT(modes_logical) 
                    ALLOCATE(mode_numbers(n_coup_modes))
                    DEALLOCATE(CDummy_array)
                    j = 1
                    DO i = 1, mode_limit
                        IF (modes_logical(i)) THEN
                            mode_numbers(j) = i
                            j = j + 1
                        END IF
                    END DO
                    DEALLOCATE(modes_logical)

                    IF(n_coup_modes > 1) mode_numbers = FLIPUD(mode_numbers)
                END IF

                ! Read number of vibrational states
                IF (INDEX(TRIM(line),'vib states') /= 0) THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 2) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, DUMMY, n_vib_states
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error in n_vib_states specification',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in n_vib_states specification',&
                                         'Check input file')
                    END IF
                END IF

                ! Read fwhm value for simulation Gaussians
                IF (INDEX(TRIM(line),'fwhm') /= 0) THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 1) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, fwhm
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error in FWHM specification',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in FWHM specification',&
                                         'Check input file')
                    END IF
                END IF

                ! Field and steps
                IF (GET_TRIMMED_VAL(line, 1, 5) == 'field') THEN

                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 3) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, field_lower, field_upper, field_step
                        
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error reading fields',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in fields specification',&
                                         'Check input file')
                    END IF

                    ! Check step is non zero
                    IF (field_step == 0.0_8 .AND. field_upper - field_lower > 0.0_8) THEN
                        CALL PRINT_ERROR('Invalid field step value',&
                                         'Check input file')
                    END IF

                    ! If B_max is smaller than B_min swap them
                    IF (field_upper < field_lower) THEN
                        RDUMMY = field_upper
                        field_upper = field_lower
                        field_lower = RDUMMY
                    END IF

                    ! If B_min less than zero error out
                    IF (field_lower < 0.0_8) THEN
                        CALL PRINT_ERROR('Invalid field value',&
                                         'Check input file')
                    END IF

                    ! Check field step is able to connect B_max and B_min 
                    ! If it is not then increase B_max so that it does
                    IF (field_upper - field_lower > 0.0_8 .AND. MODULO(field_upper - field_lower, field_step) > 0.0_8) THEN
                        field_upper = field_upper + MODULO(field_upper - field_lower, field_step)
                
                        WRITE(6,'(A)')        '!!!!       Maximum field adjusted to comply with step size        !!!!'
                        WRITE(6,'(A, F15.7, A)') '!!!!              B_max now = ', field_upper,' T             !!!!'
                    END IF
                END IF

                ! Spectrum energies and steps
                IF (GET_TRIMMED_VAL(line, 1, 6) == 'energy') THEN

                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 3) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, lower_e_bound, upper_e_bound, resolution
                        
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error reading spectrum energies',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in spectrum energy specification',&
                                         'Check input file')
                    END IF

                    ! Check resolution is non zero
                    IF (resolution == 0.0_8) THEN
                        CALL PRINT_ERROR('Invalid spectrum energy resolution',&
                                         'Check input file')
                    END IF

                    ! If E_max is smaller than E_min swap them
                    IF (upper_e_bound < lower_e_bound) THEN
                        RDUMMY = upper_e_bound
                        upper_e_bound = lower_e_bound
                        lower_e_bound = RDUMMY
                    END IF

                    ! If E_min less than zero error out
                    IF (lower_e_bound < 0.0_8) THEN
                        CALL PRINT_ERROR('Invalid spectrum energy value',&
                                         'Check input file')
                    END IF

                END IF


                ! Read fwhm value for peaks
                IF (INDEX(TRIM(line),'zcw') /= 0) THEN
                        ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 1) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, zcw_val
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error in ZCW specification',&
                                             'Check input file')
                        END IF
                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in ZCW specification',&
                                         'Check input file')
                    END IF
                END IF

                ! Magnetic field on single axis
                IF (INDEX(TRIM(line), 'single axis') /= 0) THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 2) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, single_axis
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error in single axis specification',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in single axis specification',&
                                         'Check input file')
                    END IF
                END IF

                ! Magnetic field on single axis
                IF (INDEX(TRIM(line), 'temperature') /= 0) THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 1) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, temperature
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error in temperature specification',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in temperature specification',&
                                         'Check input file')
                    END IF
                    IF (temperature < 0.0_8) THEN
                        CALL PRINT_ERROR('Negative temperature specified',&
                                         'Check input file')
                    END IF
                END IF


                ! Scale electronic transition probability
                IF (INDEX(TRIM(line), 'scale electronic') /= 0) THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 2) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, DUMMY, elec_scale
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error in scale electronic specification',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in scale electronic specification',&
                                         'Check input file')
                    END IF
                END IF


                ! Scale spin-phonon coupling values
                IF (INDEX(TRIM(line), 'scale coupling') /= 0) THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 2) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, DUMMY, coupling_scale
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error in scale coupling specification',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in scale coupling specification',&
                                         'Check input file')
                    END IF
                END IF                

                ! Equilibrium CFPs file name
                IF (INDEX(TRIM(line), 'eq cfps file') /= 0) THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 3) THEN
                        READ(p_line,*, IOSTAT = reason) DUMMY, DUMMY, DUMMY, CFPs_file
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error in eq cfps file specification',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in eq cfps file specification',&
                                         'Check input file')
                    END IF
                END IF

                ! CFP polynomials file name
                IF (INDEX(TRIM(line), 'polynomials file') /= 0) THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 2) THEN
                        READ(p_line,*, IOSTAT = reason) DUMMY, DUMMY, polynomials_file
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error in polynomials file specification',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in polynomials file specification',&
                                         'Check input file')
                    END IF
                END IF 

                ! Mode energies file name
                IF (INDEX(TRIM(line), 'mode energies file') /= 0) THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 3) THEN
                        READ(p_line,*, IOSTAT = reason) DUMMY, DUMMY, DUMMY, mode_energies_file
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error in mode energies file specification',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in mode energies file specification',&
                                         'Check input file')
                    END IF
                END IF 

                ! Replace CF energies with those from file
                IF (INDEX(TRIM(line),'replace cf energies') /= 0) THEN
                    new_cf_energies = .TRUE.
                END IF

                ! Mask off and skip purely electronic and vibrational transitions
                IF (INDEX(TRIM(line),'mask transitions') /= 0) THEN
                    mask_trans = .TRUE.
                END IF

                ! If true, OEFs are susbsumed into eq cfps
                IF (INDEX(TRIM(line),'nooef') /= 0) THEN
                    nooef = .TRUE.
                END IF

                ! Transition probability treatment
                IF (INDEX(TRIM(line), 'transition approach') /= 0) THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 2) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, DUMMY, trans_treat
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error in transition approach specification',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in transition approach specification',&
                                         'Check input file')
                    END IF
                END IF

                ! CFP polynomials file name
                IF (INDEX(TRIM(line), 'ir grid') /= 0) THEN
                    ! Check correct number of inputs are present
                    IF (CHECK_SPACES(TRIM(ADJUSTL(line))) == 2) THEN
                        READ(line,*, IOSTAT = reason) DUMMY, DUMMY, ir_grid_size
                        ! Check file is not corrupted or too short
                        IF (reason > 0)  THEN
                            CALL PRINT_ERROR('Error in ir grid specification',&
                                             'Check input file')
                        END IF

                    ELSE 
                        ! Write error message
                        CALL PRINT_ERROR('Error in ir grid specification',&
                                         'Check input file')
                    END IF
                END IF

            END IF
        END DO

        CLOSE(33)

        ! Check for missing or non-physical input variables

        IF (ion == "XX") THEN
            CALL PRINT_ERROR('No ion specified',&
                             'Check input file')
        END IF

        IF (abs(ox_state) > 4) THEN
            CALL PRINT_ERROR('Unsupported oxidation state',&
                             'Check input file')
        END IF

        IF (temperature == -1) THEN
            CALL PRINT_ERROR('Temperature not specified',&
                             'Check input file')
        END IF

        IF (n_modes == -1) THEN
            CALL PRINT_ERROR('Number of modes not specified',&
                             'Check input file')
        END IF

        IF (n_coup_modes == -1) THEN
            CALL PRINT_ERROR('Coupling modes not specified',&
                             'Check input file')
        END IF

        IF (field_lower == -1 .OR. field_upper == -1 .OR. field_step == -1) THEN
            CALL PRINT_ERROR('Magnetic field not specified',&
                             'Check input file')
        END IF

        IF (lower_e_bound == -1 .OR. upper_e_bound == -1 .OR. resolution == -1) THEN
            CALL PRINT_ERROR('Simulation energy scale not specified',&
                             'Check input file')
        END IF        


        ! Check for errors
        DO i = 1, n_coup_modes
            IF (mode_numbers(i) > n_modes) THEN
                WRITE(error_message,'(A, I4, A)') 'Invalid mode index ', IDUMMY1,' given in remove modes'
                CALL PRINT_ERROR(error_message, 'Check input file')
            END IF
        END DO

        ! Check files exist

        INQUIRE(FILE = TRIM(CFPs_file), EXIST = file_exists)
        IF (.NOT. file_exists ) THEN
            CALL ERROR_HEADER('Cannot find '//TRIM(CFPs_file), 6)
            STOP
        END IF

        INQUIRE(FILE = TRIM(polynomials_file), EXIST = file_exists)
        IF (.NOT. file_exists ) THEN
            CALL ERROR_HEADER('Cannot find '//TRIM(polynomials_file), 6)
            STOP
        END IF
        
        INQUIRE(FILE = TRIM(mode_energies_file), EXIST = file_exists)
        IF (.NOT. file_exists ) THEN
            CALL ERROR_HEADER('Cannot find '//TRIM(mode_energies_file), 6)
            STOP
        END IF

        IF (new_cf_energies) THEN
            INQUIRE(FILE = 'eq_cf_energies.dat', EXIST = file_exists)
            IF (.NOT. file_exists ) THEN
                CALL ERROR_HEADER('Cannot find eq_cf_energies.dat', 6)
                STOP
            END IF
        END IF




    END SUBROUTINE READ_INPUT_FILE

    FUNCTION GET_TRIMMED_VAL(str, loc_1, loc_2) RESULT(str_out)
        !  Returns the substring located at loc_1:loc_2 of str after
        !  str has been trimmed and adjustled
        IMPLICIT NONE
        INTEGER, INTENT(IN)                :: loc_1, loc_2
        CHARACTER(LEN = *), INTENT(IN)     :: str
        CHARACTER(LEN = loc_2 - loc_1 + 1) :: str_out
        CHARACTER(LEN = LEN(str))          :: str_tmp

        str_tmp = TRIM(ADJUSTL(str))

        IF (loc_2 - loc_1 + 1 > LEN_TRIM(ADJUSTL(str))) THEN
            ! WRITE(6,'(A)') 'Error in FUNCTION GET_TRIMMED_VAL'
            ! WRITE(6,'(A)') 'Line is:'
            ! WRITE(6,'(A)') str
            ! WRITE(6,'(A)') 'ABORTING'
            ! STOP
            str_out = ''
        END IF

        str_out = str_tmp(loc_1:loc_2)

    END FUNCTION GET_TRIMMED_VAL

FUNCTION CHECK_SPACES(str) RESULT(num_spaces)
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: str
    CHARACTER(LEN = LEN(str))      :: keep_str
    INTEGER                        :: num_spaces
                                                                  
    ! Move extra spaces and commas
    keep_str = replace_space_comma(str)

    ! Now count number of spaces
    num_spaces = count_char(keep_str, ' ')

END FUNCTION CHECK_SPACES

FUNCTION count_char (str, expr) RESULT(num)
    ! counts number of times expr occurs in str
    IMPLICIT NONE
    INTEGER                        :: i, num
    CHARACTER(LEN = *), INTENT(IN) :: str, expr
    
    num = 0
    DO i=1,len(str)-(len(expr)-1)
        IF (str(i:i+len(expr)-1) == expr) THEN
            num = num + 1
        END IF
    END DO

END FUNCTION count_char

FUNCTION replace_space_comma(str) RESULT(out_str)
    IMPLICIT NONE
    CHARACTER(LEN = *)        :: str
    CHARACTER(LEN = LEN(str)) :: out_str

    out_str = str

    ! Replace triple space with single space
    ! out_str = Replace_Text(trim(adjustl(out_str)),'   ',' ', count_char(TRIM(ADJUSTL(out_str)), '   '))
    
    ! Replace double space with single space
    ! out_str = Replace_Text(trim(adjustl(out_str)),'  ',' ', count_char(TRIM(ADJUSTL(out_str)), '  '))

    ! Replace ' ,'
    out_str = Replace_Text(trim(adjustl(out_str)),' ,',',', &
                           count_char(TRIM(ADJUSTL(out_str)), ' ,'))

    ! Replace ','
    out_str = Replace_Text(trim(adjustl(out_str)),',',' ', &
                           count_char(TRIM(ADJUSTL(out_str)), ','))

    ! Replace triple space with single space
    out_str = Replace_Text(trim(adjustl(out_str)),'   ',' ', &
                           count_char(TRIM(ADJUSTL(out_str)), '   '))
    
    ! Replace double space with single space
    out_str = Replace_Text(trim(adjustl(out_str)),'  ',' ', &
                           count_char(TRIM(ADJUSTL(out_str)), '  '))

END FUNCTION replace_space_comma

FUNCTION replace_text (str, old_expr, new_expr, num_occ) RESULT(str_mod)
    ! Replaces old_expr in str with new_expr
    IMPLICIT NONE
    INTEGER                                                                 :: i, num_occ
    CHARACTER(LEN = *), INTENT(IN)                                          :: str, old_expr, &
                                                                               new_expr
    CHARACTER(LEN = LEN(str))                                               :: str_keep
    CHARACTER(LEN = LEN(str)+(num_occ*ABS(LEN(old_expr) - LEN(new_expr))))  :: str_mod

    str_mod = str

        DO i=1,len(str_keep)-(len(old_expr)-1)
            str_keep = str_mod
            IF (str_keep(i:i+len(old_expr)-1) == old_expr) THEN
                str_mod = str_keep(:i-1)//new_expr//str_keep(i+len(old_expr):)
            END IF
        END DO

END FUNCTION replace_text

    SUBROUTINE ERROR_HEADER(title, unit)
        ! Writes an error header to unit
        ! Three lines
        ! !!!!!!!!!
        ! ! title !
        ! !!!!!!!!!
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(IN) :: title
        INTEGER, INTENT(IN)            :: unit
        INTEGER                        :: n_l_spaces, n_r_spaces, width

        IF (LEN_TRIM(title) > 60) THEN
            width = LEN_TRIM(title) + 20
        ELSE
            width = 70
        END IF

        n_l_spaces = (width - LEN_TRIM(title)) / 2
        n_r_spaces = (width - LEN_TRIM(title)) / 2

        IF (MODULO(LEN_TRIM(title),2) /= 0) THEN
            n_l_spaces = n_l_spaces + 1
            width = width
        END IF

        WRITE(unit,'(A)')   REPEAT('!',width+10)
        WRITE(unit, '(5A)') REPEAT('!',5), REPEAT(' ',n_l_spaces), TRIM(title), &
                            REPEAT(' ',n_r_spaces), REPEAT('!',5)
        WRITE(unit,'(A)')   REPEAT('!',width+10)

    END SUBROUTINE ERROR_HEADER


    SUBROUTINE ELECTRONIC_OPTIONS(ion, OEF, ox_state, elec_dim, gJ)
        ! Sets OEFs, elec_dim, and gJ using ion name and oxidation state
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(OUT)                 :: OEF(3)
        CHARACTER(LEN = *), INTENT(IN)              :: ion
        INTEGER, INTENT(IN)                         :: ox_state
        INTEGER, INTENT(OUT)                        :: elec_dim
        REAL(KIND = 8)                              :: S, J, L
        REAL(KIND = 8), INTENT(OUT)                 :: gJ
        INTEGER                                     :: num_f_elec

        ! Open output file
        OPEN(66, FILE = 'firms_sim.out', STATUS = 'UNKNOWN', POSITION = 'APPEND')

        ! Write ion data section header
        WRITE(66,'(A)') ''

        CALL SECTION_HEADER('Metal Ion Data', 66)
        WRITE(66,'(A)') ''

        ! Write ion symbol
        WRITE(66,'(3A,I0,A)') 'Ion = ',TRIM(ion),'(',ox_state,')'

        ! Calculate number of f electrons
        IF (ion == 'ce') num_f_elec =  5
        IF (ion == 'pr') num_f_elec =  6
        IF (ion == 'nd') num_f_elec =  7
        IF (ion == 'sm') num_f_elec =  8
        IF (ion == 'eu') num_f_elec =  9
        IF (ion == 'gd') num_f_elec = 10
        IF (ion == 'tb') num_f_elec = 11
        IF (ion == 'dy') num_f_elec = 12
        IF (ion == 'ho') num_f_elec = 13
        IF (ion == 'er') num_f_elec = 14
        IF (ion == 'tm') num_f_elec = 15
        IF (ion == 'yb') num_f_elec = 16

        num_f_elec = num_f_elec - ox_state

        ! Write f electron count
        WRITE(66,'(I0, A)') num_f_elec, ' f electrons'

        ! Calculate S
        IF (num_f_elec == 7 .OR. num_f_elec == 14) THEN
            CALL PRINT_ERROR('No orbital moment! ','')
        ELSE IF (num_f_elec < 7) THEN
            S = real(num_f_elec, 8) / 2.0_8
        ELSE IF (num_f_elec > 7) THEN
            S = (14.0_8 - num_f_elec)  / 2.0_8
        END IF

        ! Write S to output file
        IF (S - 0.5_8 == INT(S)) THEN ! Non Integer S
            WRITE(66,'(A, I0, A)') 'S = ', INT(S*2.0_8),'/2'
        ELSE
            WRITE(66,'(A, I0)') 'S = ', INT(S) ! Integer S
        END IF

        ! Calculate L
        IF (num_f_elec == 1 .OR. num_f_elec == 6 .OR. num_f_elec == 8 .OR. num_f_elec == 13) THEN
            L = 3.0_8
        ELSE IF (num_f_elec == 2 .OR. num_f_elec == 5 .OR. num_f_elec == 9 .OR. num_f_elec == 12) THEN
            L = 5.0_8
        ELSE IF (num_f_elec == 3 .OR. num_f_elec == 4 .OR. num_f_elec == 10 .OR. num_f_elec == 11) THEN
            L = 6.0_8
        END IF

        ! Write L to output file
        WRITE(66,'(A, I0)') 'L = ', INT(L)

        ! Calculate J
        IF (num_f_elec < 7) THEN
            J = ABS(L - S)
        ELSE IF (num_f_elec > 7) THEN
            J = L + S
        END IF

        ! Write J to output file
        IF (INT(J) == INT(J*2.0_8)/2) THEN ! Non Integer J
            WRITE(66,'(A, I0, A)') 'J = ', INT(J*2.0_8),'/2'
        ELSE
            WRITE(66,'(A, I0)') 'J = ', INT(J) ! Integer J
        END IF

        ! Calculate elec_dim
        elec_dim = NINT(2.0_8 * J) + 1

        ! Calculate gJ (Lande g factor) for a given L, S, J
        gJ = 1.5_8 + (S*(S + 1.0_8) - L*(L + 1.0_8))/(2.0_8*J*(J + 1.0_8))

        ! Write gJ to output file
        WRITE(66,'(A, F7.5)') 'gJ = ', gJ
        WRITE(66,*)

        ! Set operator equivalent factors
        IF (ion == 'ce' .AND. ox_state == 3) THEN  
            OEF(1) =    -2.0_8/35.0_8
            OEF(2) =    2.0_8/315.0_8
            OEF(3) =    0.0_8
        ELSE IF (ion == 'pr' .AND. ox_state == 3) THEN  
            OEF(1) =    -52.0_8/2475.0_8
            OEF(2) =    -4.0_8/5445.0_8
            OEF(3) =    272.0_8/4459455.0_8
        ELSE IF (ion == 'nd' .AND. ox_state == 3) THEN  
            OEF(1) =    -7.0_8/1089.0_8
            OEF(2) =    -136.0_8/467181.0_8
            OEF(3) =    -1615.0_8/42513471.0_8
        ELSE IF (ion == 'pm' .AND. ox_state == 3) THEN  
            OEF(1) =    14.0_8/1815.0_8
            OEF(2) =    952.0_8/2335905.0_8
            OEF(3) =    2584.0_8/42513471.0_8
        ELSE IF (ion == 'sm' .AND. ox_state == 3) THEN  
            OEF(1) =    13.0_8/315.0_8
            OEF(2) =    26.0_8/10395.0_8
            OEF(3) =    0.0_8
        ELSE IF (ion == 'eu' .AND. ox_state == 3) THEN  
            CALL PRINT_ERROR('Eu3+ has J=0 and hence no mJ states','')
        ELSE IF (ion == 'gd' .AND. ox_state == 3) THEN  
            CALL PRINT_ERROR('Gd3+ has no orbital angular momentum','')
        ELSE IF (ion == 'tb' .AND. ox_state == 3) THEN  
            OEF(1) =    -1.0_8/99.0_8
            OEF(2) =    2.0_8/16335.0_8
            OEF(3) =    -1.0_8/891891.0_8
        ELSE IF (ion == 'dy' .AND. ox_state == 3) THEN  
            OEF(1) =    -2.0_8/315.0_8
            OEF(2) =    -8.0_8/135135.0_8
            OEF(3) =    4.0_8/3864861.0_8
        ELSE IF (ion == 'ho' .AND. ox_state == 3) THEN  
            OEF(1) =    -1.0_8/450.0_8
            OEF(2) =    -1.0_8/30030.0_8
            OEF(3) =    -5.0_8/3864861.0_8
        ELSE IF (ion == 'er' .AND. ox_state == 3) THEN  
            OEF(1) =    4.0_8/1575.0_8
            OEF(2) =    2.0_8/45045.0_8
            OEF(3) =    8.0_8/3864861.0_8
        ELSE IF (ion == 'tm' .AND. ox_state == 3) THEN  
            OEF(1) =    1.0_8/99.0_8
            OEF(2) =    8.0_8/49005.0_8
            OEF(3) =    -5.0_8/891891.0_8
        ELSE IF (ion == 'yb' .AND. ox_state == 3) THEN  
            OEF(1) =    2.0_8/63.0_8
            OEF(2) =    -2.0_8/1155.0_8
            OEF(3) =    4.0_8/27027.0_8
        ELSE
            CALL PRINT_ERROR('!!!!                Atom and oxidation state not supported!                 !!!!','')
        END IF

        ! Close output file
        CLOSE(66)

    END SUBROUTINE ELECTRONIC_OPTIONS

    SUBROUTINE PHONON_OPTIONS(n_coup_modes, mode_energies, mode_energies_file, &
                              num_modes, n_vib_states, pho_dim, mode_numbers, dip_deriv)
        IMPLICIT NONE
        INTEGER, INTENT(IN)                      :: n_coup_modes, num_modes, n_vib_states, &
                                                    mode_numbers(:)
        INTEGER, INTENT(OUT)                     :: pho_dim
        INTEGER                                  :: i, col, n
        REAL(KIND = 8), ALLOCATABLE              :: temporary(:,:), pho_diff(:,:)
        REAL(KIND = 8), INTENT(OUT), ALLOCATABLE :: mode_energies(:), dip_deriv(:,:)
        CHARACTER(LEN = *), INTENT(IN)           :: mode_energies_file

        ! e.g. If n_vib_states == 2 then n=0 and n=1 states used

        pho_dim = n_vib_states ** n_coup_modes

        ! Allocate and zero mode_energies array
        ALLOCATE(mode_energies(num_modes))
        mode_energies = 0.0_8

        ! Read mode energies
        OPEN(33, FILE = mode_energies_file, STATUS = 'OLD')

            DO i = 1, num_modes
                READ(33,*) mode_energies(i)
            END DO

        CLOSE(33)

        ! Calculate which modes are degenerate, and how many different degeneracies there are
        ALLOCATE(pho_diff(n_coup_modes, n_coup_modes))
        pho_diff = 100.0_8
        do i = 2, n_coup_modes
            do j = 1, i-1
                pho_diff(j,i) = abs(mode_energies(i) - mode_energies(j))
            end do
        end do


        ! Allocate and zero dipole derivatives array
        ALLOCATE(dip_deriv(n_coup_modes, 3))
        ALLOCATE(temporary(num_modes, 3))
        dip_deriv = 0.0_8

        IF (TRIM(trans_treat) /= 'simple') THEN

            ! Read in dipole derivatives
            OPEN(33, FILE = 'dipole_derivatives.dat', STATUS = 'OLD')

                DO i = 1, num_modes
                    READ(33,*) (temporary(i, col), col = 1, 3)
                END DO

            CLOSE(33)

            n = 1
            DO i = 1, n_coup_modes
                dip_deriv(n, :) = temporary(mode_numbers(i), :)
                n = n + 1
            END DO

        END IF

!        OPEN(33, FILE = 'hessian.dat', STATUS = 'OLD')

!        DO i = 1,


!        CLOSE(33)


    END SUBROUTINE PHONON_OPTIONS

    SUBROUTINE CALC_G_WIDTH(FWHM, g_width, section)
        IMPLICIT NONE
        REAL(KIND = 8)                 :: FWHM
        REAL(KIND = 8), INTENT(OUT)    :: g_width
        CHARACTER(LEN = *)              :: section

    ! Open output file
        OPEN(66, FILE = "firms_sim.out", STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! Calculate Gaussian width using FWHM in cm-1
    ! $\sigma = \frac{\text{FWHM}}{2\sqrt{2\log(2)}}$
        g_width = FWHM/(2.0_8*sqrt(2.0_8*log(2.0_8)))

    ! Write FWHM info to output file
        WRITE(66,'(A)') ''
        CALL SECTION_HEADER(TRIM(section),66)

        WRITE(66,*)
        WRITE(66,'(A, F8.3, A)') 'FWHM            = ', FWHM, ' cm^-1'
        WRITE(66,'(A, F8.3, A)') 'Gaussian Width  = ', g_width, ' cm^-1'

    ! Close output file
        CLOSE(66)
    END SUBROUTINE CALC_G_WIDTH

    SUBROUTINE COUPLING_OPTIONS(pho_dim, elec_dim, coup_dim)
        IMPLICIT NONE
        INTEGER, INTENT(IN)  :: pho_dim, elec_dim
                                
        INTEGER, INTENT(OUT) :: coup_dim

        IF (pho_dim == 0) THEN
            coup_dim = elec_dim
        ELSE
            coup_dim = elec_dim * pho_dim
        END IF

    END SUBROUTINE COUPLING_OPTIONS

    SUBROUTINE FIELD_OPTIONS(field_lower, field_upper, field_step, field_vecs, field_angs, &
                             field_strengths, n_field_strengths, n_field_vecs, &
                             zcw_val, single_axis, ir_grid_size)
        use constants
        REAL(KIND = 8), INTENT(IN)               :: field_lower, field_upper, field_step
        REAL(KIND = 8), INTENT(OUT), ALLOCATABLE :: field_strengths(:), field_vecs(:,:,:), &
                                                    field_angs(:,:)
        REAL(KIND = 8), ALLOCATABLE              :: raw_vecs(:,:)
        INTEGER, INTENT(OUT)                     :: n_field_vecs, n_field_strengths
        INTEGER, INTENT(IN)                      :: zcw_val, ir_grid_size
        INTEGER                                  :: i
        CHARACTER(LEN = *)                       :: single_axis

        ! Calculate field strengths using, upper, lower and step values
        
        IF (field_upper == field_lower) THEN
            n_field_strengths = 1
        ELSE 
            n_field_strengths = 1 + INT((field_upper - field_lower) / field_step)
        END IF

        ALLOCATE(field_strengths(n_field_strengths))
        field_strengths = 0.0_8

        field_strengths(1) = field_lower

        IF (n_field_strengths > 1) THEN
            DO i = 2, n_field_strengths
                field_strengths(i) = field_strengths(i - 1) + field_step
                ! Manually set zero field to small field
                IF (field_strengths(i) == 0.0_8) THEN
                    field_strengths(i) = 0.00002
                END IF
            END DO
        END IF

        ! Set field vectors to one direction
        IF      (TRIM(single_axis) == 'x') THEN
            ALLOCATE(raw_vecs(1,3), field_angs(1,2))
            field_angs(1,:)     = [0.0_8, pi/2]
            raw_vecs(1,:)       = [1.0_8, 0.0_8, 0.0_8]
            n_field_vecs      = 1
        ELSE IF (TRIM(single_axis) == 'y') THEN
            ALLOCATE(raw_vecs(1,3), field_angs(1,2))
            field_angs(1,:)     = [pi/2, pi/2]
            raw_vecs(1,:)       = [0.0_8, 1.0_8, 0.0_8]
            n_field_vecs      = 1
        ELSE IF (TRIM(single_axis) == 'z') THEN
            ALLOCATE(raw_vecs(1,3), field_angs(1,2))
            field_angs(1,:)     = [0.0_8, 0.0_8]
            raw_vecs(1,:)       = [0.0_8, 0.0_8, 1.0_8]
            n_field_vecs      = 1
        ELSE
            ! or calculate field vectors for averaging using ZCW scheme
            CALL ZCW(zcw_val, raw_vecs, n_field_vecs, field_angs)
        END IF

        ! Apply raw_vecs to field strengths to give field_vecs
        ALLOCATE(field_vecs(n_field_strengths, n_field_vecs, 3))
        field_vecs = 0.0_8
        DO i = 1, n_field_strengths
            field_vecs(i,:,:) = raw_vecs * field_strengths(i)
        END DO

        ! Open output file
        OPEN(66, FILE = 'firms_sim.out', STATUS = 'UNKNOWN', POSITION = 'APPEND')

            ! Write equilbrium electronic structure section header
            WRITE(66,'(A)') ''
            CALL SECTION_HEADER('Magnetic Field Information', 66)

            WRITE(66,'(A)') ''   
            CALL SUB_SECTION_HEADER('Magnetic Field Strengths (T)', 66)


            DO i = 1, n_field_strengths
                WRITE(66,'(F8.4)') field_strengths(i)
            END DO
            WRITE(66,'(A)') ''   


            WRITE(66,'(A,I0,A)') 'Each field strength is powder-averaged over ', n_field_vecs, ' positions'
            WRITE(66,'(A)') ''   

            WRITE(66,'(A,I0,A)') 'IR polarisation averaged over ', ir_grid_size, ' positions for each field position'
            WRITE(66,'(A)') ''   


        CLOSE(66)


    END SUBROUTINE FIELD_OPTIONS

    SUBROUTINE ZCW(m, VecDir, NumDir, angles)
        use constants
        IMPLICIT NONE
        INTEGER                                   :: i, c(3)
        INTEGER, INTENT(IN)                       :: m
        INTEGER, INTENT(OUT)                      :: NumDir
        INTEGER, ALLOCATABLE                      :: g(:)
        REAL(KIND = 8), ALLOCATABLE, INTENT(OUT)  :: VecDir(:,:), angles(:,:)
        REAL(KIND = 8)                            :: a, b
        
        ALLOCATE(g(m+3))
        c(1) = -1
        c(2) = 1
        c(3) = 1
        g(1) = 8
        g(2) = 13

        DO i=3,m+3
            g(i) = g(i-1) + g(i-2)
        END DO

        NumDir = g(m+3)

        ALLOCATE(VecDir(NumDir,3), angles(NumDir,2))

        DO i=1,NumDir
            a = (2.0_8*pi/real(c(3),8))*mod(real(i-1,8)*real(g(m+1),8)/NumDir,1.0_8)
            b = acos(real(c(1),8)*((real(c(2),8)*mod(real(i-1,8)/NumDir,1.0_8))-1.0_8))
            angles(i,1) = a
            angles(i,2) = b
            VecDir(i,1) = sin(b)*cos(a)
            VecDir(i,2) = sin(b)*sin(a)
            VecDir(i,3) = cos(b)
        END DO

        DEALLOCATE(g)

    END SUBROUTINE ZCW


    SUBROUTINE CALC_EQ_CF_HAMILTONIAN(CFP_file, elec_dim, EQ_H_CF, CF_vecs, CF_vals, gJ, &
                                      single_axis, nooef, OEF, new_cf_energies, B_quant)
        USE stev_ops
        IMPLICIT NONE
        CHARACTER(LEN = *), INTENT(IN)              :: CFP_file, single_axis
        INTEGER, INTENT(IN)                         :: elec_dim
        COMPLEX(KIND = 8), INTENT(OUT), ALLOCATABLE :: EQ_H_CF(:,:)
        REAL(KIND = 8), INTENT(IN)                  :: gJ, OEF(3)
        REAL(KIND = 8), INTENT(OUT)                 :: B_quant
        REAL(KIND = 8), ALLOCATABLE                 :: CFPs(:,:), CF_vals(:)
        COMPLEX(KIND = 8), ALLOCATABLE              :: stevens_ops(:,:,:,:), CF_vecs(:,:), H_ZEE(:,:)
        LOGICAL, INTENT(IN)                         :: nooef, new_cf_energies

        ! Allocate CFPs array
        ALLOCATE(CFPs(3,13))
        CFPs = 0.0_8

        ! Read in Equilibrium CFPs
        CALL READ_CFPs(CFP_file, CFPs)

        ! Calculate stevens operators
        CALL GET_STEVENS_OPERATORS(elec_dim - 1, stevens_ops)

        ! Allocate and zero Hamiltonian array
        ALLOCATE(EQ_H_CF(elec_dim, elec_dim),H_ZEE(elec_dim, elec_dim))
        EQ_H_CF = (0.0_8, 0.0_8)
        H_ZEE = (0.0_8, 0.0_8)

        ! Add small Zeeman perturbation to quantise kd_percents
        B_quant = 0.000001_8 ! Tesla

        IF (single_axis == 'x') THEN
            CALL CALC_H_ZEE(B_quant, 0.0_8, 0.0_8, gJ, H_ZEE)
        ELSE IF (single_axis == 'y') THEN
            CALL CALC_H_ZEE(0.0_8, B_quant, 0.0_8, gJ, H_ZEE)
        ELSE
            CALL CALC_H_ZEE(0.0_8, 0.0_8, B_quant, gJ, H_ZEE)
        END IF

        ! Calculate CF Hamiltonian 
        IF (nooef) THEN ! OEFs subsumed into parameters
            EQ_H_CF = H_CF(CFPs, stevens_ops, 6) + H_ZEE
        ELSE ! OEFs must be included explicitly
            EQ_H_CF = H_CF(CFPs, stevens_ops, OEF, 6) + H_ZEE
        END IF

        ! Allocate arrays for eigenvectors and eigenvalues
        ALLOCATE(CF_vecs(elec_dim, elec_dim), CF_vals(elec_dim))
        CF_vecs  = (0.0_8, 0.0_8)
        CF_vals  = 0.0_8

        ! Diagonalise CF Hamiltonian
        CALL DIAGONALISE('U',EQ_H_CF,CF_vecs,CF_vals)

        ! Set lowest eigenvalue to zero
        CF_vals = CF_vals - CF_vals(1)

        IF (new_cf_energies) THEN
            CALL READ_CF_SHIFT(CF_vals)
            ! Add on small zeeman perturbation to avoid degeneracies
            CF_vals = CF_vals + REAL(DIAG(H_ZEE), 8)
        END IF

    END SUBROUTINE CALC_EQ_CF_HAMILTONIAN

    SUBROUTINE READ_CF_SHIFT(CF_vals)
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(OUT) :: CF_vals(:)
        INTEGER                     :: i

        OPEN(33, FILE = 'eq_cf_energies.dat', STATUS='UNKNOWN')

            DO i = 1, SIZE(CF_vals)
                READ(33, *) CF_vals(i)
            END DO

        CLOSE(33)

    END SUBROUTINE READ_CF_SHIFT

    SUBROUTINE READ_CFPs(CFP_file, CFPs)
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(OUT)    :: CFPs(:,:)
        CHARACTER(LEN = *), INTENT(IN) :: CFP_file
        REAL(KIND=8)                   :: DUMMY
        INTEGER                        :: i
        LOGICAL                        :: PHI

        ! Reads in crystal field parameters from file
        ! can be as a list of parameters or in phi input style
        ! i.e 1 k q param

        ! Check if first line contains a 1 on its own, if it does then read using phi input style
        OPEN(33, FILE = TRIM(CFP_file), STATUS = 'old')

            READ(33,*) DUMMY

            IF (DUMMY == 1.0_8) THEN
                PHI = .TRUE.
            ELSE
                PHI = .FALSE.
            END IF

            REWIND(33)

            IF (PHI) THEN 

                DO i = 1, 5
                    READ(33, *) DUMMY, DUMMY, DUMMY, CFPs(1,i)
                END DO

                DO i = 1, 9
                    READ(33, *) DUMMY, DUMMY, DUMMY, CFPs(2,i)
                END DO

                DO i = 1, 13
                    READ(33, *) DUMMY, DUMMY, DUMMY, CFPs(3,i)
                END DO

            ELSE

                DO i = 1, 5
                    READ(33, *) CFPs(1,i)
                END DO

                DO i = 1, 9
                    READ(33, *) CFPs(2,i)
                END DO

                DO i = 1, 13
                    READ(33, *) CFPs(3,i)
                END DO

            END IF

        CLOSE(33)

    END SUBROUTINE READ_CFPs

    SUBROUTINE CALC_EQ_PHO_HAMILTONIAN(n_coup_modes, mode_energies, mode_numbers, &
                                       n_vib_states, pho_dim, EQ_H_pho_summed)
        USE matrix_tools
        ! Calculates equilibrium phonon Hamiltonian
        ! where all modes are coupled together
        IMPLICIT NONE
        INTEGER, INTENT(IN)                       :: n_coup_modes, n_vib_states, pho_dim, mode_numbers(:)
        INTEGER                                   :: j, n
        REAL(KIND = 8), INTENT(IN)                :: mode_energies(:)
        REAL(KIND = 8), ALLOCATABLE               :: EQ_H_pho(:,:,:)
        REAL(KIND = 8), INTENT(OUT), ALLOCATABLE  :: EQ_H_pho_summed(:,:)


        ! Allocate and zero phonon Hamiltonian and temporary array for uncoupled Hamiltonians
        ALLOCATE(EQ_H_pho(n_coup_modes, n_vib_states, n_vib_states), EQ_H_pho_summed(pho_dim, pho_dim))
        EQ_H_pho        = 0.0_8
        EQ_H_pho_summed = 0.0_8

        ! Calculate phonon Hamiltonian
        ! Diagonal matrix whose entries are the energies of each state

        DO j = 1, n_coup_modes
            DO n = 1, n_vib_states
                EQ_H_pho(j, n, n) = (real(n-1, 8)+0.5_8) * mode_energies(mode_numbers(j))
            END DO
        END DO

        IF (n_coup_modes > 1) THEN
            CALL COUPLE_MODE_ARRAY(EQ_H_pho, EQ_H_pho_summed, n_coup_modes, n_vib_states)
        ELSE
            EQ_H_pho_summed = EQ_H_pho(1,:,:)
        END IF

    END SUBROUTINE CALC_EQ_PHO_HAMILTONIAN

    SUBROUTINE COUPLE_MODE_ARRAY(matrix, coupled_matrix, n_coup_modes, n_vib_states)
        USE matrix_tools
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)   :: matrix(:,:,:)
        REAL(KIND = 8), INTENT(OUT)  :: coupled_matrix(:,:)
        INTEGER, INTENT(IN)          :: n_coup_modes, n_vib_states
        INTEGER                      :: ident_right_size, ident_left_size, j
        INTEGER, ALLOCATABLE         :: lhs(:), rhs(:)

        ! Arrays to keep track of size of identity arrays kroneckered either side of a given Hamiltonian
        ALLOCATE(lhs(n_coup_modes))
        lhs  = [(j, j = 0, n_coup_modes - 1)]

        ALLOCATE(rhs(n_coup_modes))
        rhs  = [(j, j = n_coup_modes - 1, 0, -1)]

        DO j = 1, n_coup_modes
            ident_right_size = 0
            ident_left_size  = 0

            ! Create left and right identities
            ! If first or last mode then no need for left or right identities
            IF (j /= 1) THEN
                ident_left_size  = n_vib_states ** lhs(j)
            END IF
            IF (j /= n_coup_modes) THEN
                ident_right_size = n_vib_states ** rhs(j)
            END IF

            ! First mode then kronecker from right with phonon identity
            IF (j == 1) THEN
                coupled_matrix = coupled_matrix + KRON_PROD(matrix(j,:,:), IDENTITY(ident_right_size))

            ! Last mode then kronecker from left with phonon identity, but nothing from right
            ELSE IF (j == n_coup_modes) THEN
                coupled_matrix = coupled_matrix + KRON_PROD(IDENTITY(ident_left_size), matrix(j,:,:))

            ! All other mode_numbers kronecker from left with  phonon identities
            ! and from right with remaining phonon identities
            ELSE
                coupled_matrix = coupled_matrix + KRON_PROD(KRON_PROD(IDENTITY(ident_left_size), matrix(j,:,:)), IDENTITY(ident_right_size))
            END IF
        END DO
    END SUBROUTINE COUPLE_MODE_ARRAY

    SUBROUTINE COUPLE_COMPLEX_MODE_ARRAY(matrix, coupled_matrix, n_coup_modes, n_vib_states)
        USE matrix_tools
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN)   :: matrix(:,:,:)
        COMPLEX(KIND = 8), INTENT(OUT)  :: coupled_matrix(:,:,:)
        INTEGER, INTENT(IN)          :: n_coup_modes, n_vib_states
        INTEGER                      :: ident_right_size, ident_left_size, j
        INTEGER, ALLOCATABLE         :: lhs(:), rhs(:)

        ! Arrays to keep track of size of identity arrays kroneckered either side of a given Hamiltonian
        ALLOCATE(lhs(n_coup_modes))
        lhs  = [(j, j = 0, n_coup_modes - 1)]

        ALLOCATE(rhs(n_coup_modes))
        rhs  = [(j, j = n_coup_modes - 1, 0, -1)]

        DO j = 1, n_coup_modes
            ident_right_size = 0
            ident_left_size  = 0

            ! Create left and right identities
            ! If first or last mode then no need for left or right identities
            IF (j /= 1) THEN
                ident_left_size  = n_vib_states ** lhs(j)
            END IF
            IF (j /= n_coup_modes) THEN
                ident_right_size = n_vib_states ** rhs(j)
            END IF

            ! First mode then kronecker from right with phonon identity
            IF (j == 1) THEN
                coupled_matrix(j,:,:) = KRON_PROD(matrix(j,:,:), IDENTITY(ident_right_size))

            ! Last mode then kronecker from left with phonon identity, but nothing from right
            ELSE IF (j == n_coup_modes) THEN
                coupled_matrix(j,:,:) = KRON_PROD(IDENTITY(ident_left_size), matrix(j,:,:))

            ! All other mode_numbers kronecker from left phonon identities
            ! and from right with remaining phonon identities
            ELSE
                coupled_matrix(j,:,:) = KRON_PROD(KRON_PROD(IDENTITY(ident_left_size), matrix(j,:,:)), IDENTITY(ident_right_size))
            END IF
        END DO
    END SUBROUTINE COUPLE_COMPLEX_MODE_ARRAY

    SUBROUTINE COUPLE_INTEGER_MODE_ARRAY(matrix, coupled_matrix, n_coup_modes, n_vib_states)
        USE matrix_tools
        IMPLICIT NONE
        INTEGER, INTENT(IN)   :: matrix(:,:,:)
        INTEGER, INTENT(OUT)  :: coupled_matrix(:,:,:)
        INTEGER, INTENT(IN)   :: n_coup_modes, n_vib_states
        INTEGER               :: ident_right_size, ident_left_size, j
        INTEGER, ALLOCATABLE  :: lhs(:), rhs(:)

        ! Arrays to keep track of size of I_IDENTITY arrays kroneckered either side of a given Hamiltonian
        ALLOCATE(lhs(n_coup_modes))
        lhs  = [(j, j = 0, n_coup_modes - 1)]

        ALLOCATE(rhs(n_coup_modes))
        rhs  = [(j, j = n_coup_modes - 1, 0, -1)]

        IF (n_coup_modes == 1) THEN
            coupled_matrix(1,:,:) = matrix(1,:,:)
            RETURN
        END IF

        DO j = 1, n_coup_modes
            ident_right_size = 0
            ident_left_size  = 0

            ! Create left and right identities
            ! If first or last mode then no need for left or right identities
            IF (j /= 1) THEN
                ident_left_size  = n_vib_states ** lhs(j)
            END IF
            IF (j /= n_coup_modes) THEN
                ident_right_size = n_vib_states ** rhs(j)
            END IF

            ! First mode then kronecker from right with phonon I_IDENTITY
            IF (j == 1) THEN
                coupled_matrix(j,:,:) = KRON_PROD(matrix(j,:,:), I_IDENTITY(ident_right_size))
            ! Last mode then kronecker from left with phonon I_IDENTITY, but nothing from right
            ELSE IF (j == n_coup_modes) THEN
                coupled_matrix(j,:,:) = KRON_PROD(I_IDENTITY(ident_left_size), matrix(j,:,:))

            ! All other mode_numbers kronecker from left phonon identities
            ! and from right with remaining phonon identities
            ELSE
                coupled_matrix(j,:,:) = KRON_PROD(KRON_PROD(I_IDENTITY(ident_left_size), matrix(j,:,:)), I_IDENTITY(ident_right_size))
            END IF
        END DO
    END SUBROUTINE COUPLE_INTEGER_MODE_ARRAY

    SUBROUTINE CALC_COUPLING_HAMILTONIAN(polynomials_file, coup_dim, n_coup_modes, &
                                         num_modes, mode_numbers, elec_dim, n_vib_states, &
                                         H_Coup_summed, OEF, coupling_scale, eq_CF_vecs)
        USE matrix_tools
        USE stev_ops
        IMPLICIT NONE
        INTEGER, INTENT(IN)                          :: n_coup_modes, mode_numbers(:), coup_dim, &
                                                        n_vib_states, elec_dim, num_modes
        INTEGER                                      :: j
        COMPLEX(KIND = 8), INTENT(IN)                :: eq_CF_vecs(:,:)
        COMPLEX(KIND = 8), INTENT(OUT), ALLOCATABLE  :: H_Coup_summed(:,:)
        COMPLEX(KIND = 8), ALLOCATABLE               :: cf_part(:,:), stevens_ops(:,:,:,:), &
                                                        H_Coup(:,:,:), pho_part(:,:), &
                                                        H_pho_int(:,:,:), pho_part_int(:,:,:)
        REAL(KIND = 8), INTENT(IN)                   :: OEF(3), coupling_scale
        REAL(KIND = 8), ALLOCATABLE                  :: dbkq_dq(:,:,:), CFP_polynomials(:,:,:,:)
        CHARACTER(LEN = *), INTENT(IN)               :: polynomials_file

        ! Allocate and zero coupling Hamiltonian
        ALLOCATE(H_Coup_summed(coup_dim, coup_dim), &
                 H_Coup(n_coup_modes, coup_dim, coup_dim), &
                 H_pho_int(n_coup_modes, n_vib_states**n_coup_modes, n_vib_states**n_coup_modes), &
                 cf_part(elec_dim,elec_dim))
        
        H_Coup_summed = (0.0_8, 0.0_8)
        H_Coup        = (0.0_8, 0.0_8)
        H_pho_int     = (0.0_8, 0.0_8)


        ! Allocate pho part of coupling Hamiltonian and dBkq/dQ values
        ALLOCATE(pho_part(n_vib_states, n_vib_states), &
                 dbkq_dq(n_coup_modes,3,13), &
                 pho_part_int(n_coup_modes, n_vib_states, n_vib_states))
        pho_part = (0.0_8, 0.0_8)

        ! Read in CFP polynomials
        CALL READ_PHONON_CF_POLYNOMIAL(polynomials_file, CFP_polynomials, num_modes)

        ! Select 1st order term of CFP polynomials for coupling modes
        DO j = 1, n_coup_modes
            dbkq_dq(j,:,:) = CFP_polynomials(mode_numbers(j), :, :, 3)
        END DO

        ! Calculate stevens operators
        CALL GET_STEVENS_OPERATORS(elec_dim - 1, stevens_ops)

        ! Calculate Phonon part of coupling Hamiltonian
        ! This is the same for every mode so can be calculated once
        CALL GET_Q_MATRIX(n_vib_states, pho_part)

        IF(n_coup_modes > 1) THEN

            ! Calculate all possible coupled phonon matrices
            DO j = 1,n_coup_modes

                pho_part_int(j,:,:) = pho_part(:,:)

            END DO
            CALL COUPLE_COMPLEX_MODE_ARRAY(pho_part_int, H_pho_int, n_coup_modes, n_vib_states)

            ! Loop over each coupled mode and calculate coupling Hamiltonian
            DO j = 1, n_coup_modes

                ! Calculate CF part of coupling Hamiltonian for current mode
                cf_part = H_CF(dbkq_dq(j,:,:), stevens_ops, OEF, 6)  * coupling_scale

                call EXPECTATION('F',eq_CF_vecs,cf_part)

                ! Form kronecker product of electronic and vibrational parts
                H_Coup(j,:,:) = KRON_PROD(cf_part, H_pho_int(j,:,:))

            END DO
            
            ! Now sum up each H_Coup for each mode

            DO j = 1, n_coup_modes

                H_Coup_summed = H_Coup_summed + H_Coup(j,:,:)

            END DO

        ELSE

            cf_part = H_CF(dbkq_dq(j,:,:), stevens_ops, OEF, 6)  * coupling_scale

            call EXPECTATION('F',eq_CF_vecs,cf_part)

            ! Form kronecker product of electronic and vibrational parts
            H_Coup_summed = KRON_PROD(cf_part, pho_part)

        END IF

        !H_Coup_summed = (0.0_8, 0.0_8)

        ! Write Coupling Hamiltonian to file
        ! CALL WRITE_ARRAY(H_Coup(1,:,:), 'H_coup.dat')

        ! IF (n_coup_modes > 1) THEN
        !     CALL WRITE_ARRAY(H_Coup(2,:,:), 'H_coup_m2.dat')
        ! END IF

    END SUBROUTINE CALC_COUPLING_HAMILTONIAN

    SUBROUTINE LABEL_BASIS(elec_dim, pho_dim, n_coup_modes, n_vib_states, coup_dim, &
                           basis_labels)
        USE matrix_tools
        INTEGER, INTENT(IN)              :: elec_dim, pho_dim, n_coup_modes, n_vib_states,&
                                            coup_dim
        INTEGER                          :: i, j, n
        INTEGER,ALLOCATABLE              :: elec(:,:), phonon(:,:,:), total(:,:,:), tot_pho(:,:,:),&
                                            big_pho(:,:,:), big_elec(:,:)
        INTEGER,ALLOCATABLE, INTENT(OUT) :: basis_labels(:,:)

        allocate(phonon(n_coup_modes,n_vib_states,n_vib_states),&
                 elec(elec_dim,elec_dim),&
                 total(n_coup_modes+1,coup_dim,coup_dim),&
                 basis_labels(coup_dim,n_coup_modes+1),&
                 tot_pho(n_coup_modes,pho_dim,pho_dim),&
                 big_elec(coup_dim, coup_dim),&
                 big_pho(n_coup_modes,coup_dim,coup_dim))

        elec = 0
        phonon = 0
        n = 0
        DO i = 1, elec_dim - 1, 2
            n = n + 1
            elec(i, i) = -n
            elec(i + 1, i + 1) = n
        END DO

        do i = 2,n_vib_states
            phonon(:,i,i) = i-1 
        end do

        ! Get electronic labels in big basis
        big_elec = KRON_PROD(elec,I_IDENTITY(pho_dim))

        ! Get phonon labels in total phonon basis - no electronic part!
        CALL COUPLE_INTEGER_MODE_ARRAY(phonon, tot_pho, n_coup_modes, n_vib_states)

        ! Get phonon labels in big basis - now including electronic part
        do j = 1,n_coup_modes
            big_pho(j,:,:) = KRON_PROD(I_IDENTITY(elec_dim), tot_pho(j,:,:))
        end do

        ! Copy electronic labels into 1st column of basis labels
        basis_labels(:,1) = DIAG(big_elec)

        ! Copy phonon labels into remaining columns of basis labels
        ! Mode 1 labels go into 2nd column, mode 2 into 3rd, etc.
        do j = 1,n_coup_modes
            basis_labels(:, j + 1) = DIAG(big_pho(j,:,:))
        end do

        ! Flip phonon labels of each electronic state to match flipped input mode numbers from user
        ! This gets around the backwards nature of the kronecker product
        do j = 1, coup_dim
            basis_labels(j,2:) = FLIPUD(basis_labels(j,2:))
        end do

        !call write_array(array=basis_labels, file='basis_labels.dat')

        deallocate(big_elec, big_pho, tot_pho, phonon)

    END SUBROUTINE LABEL_BASIS

    SUBROUTINE CALC_H_EQ_TOTAL(elec_dim, pho_dim, coup_dim, EQ_H_CF, EQ_H_pho, H_Coup, &
                               H_total, H_total_NC)
        USE matrix_tools
        IMPLICIT NONE
        INTEGER, INTENT(IN)                          :: elec_dim, pho_dim, coup_dim
        REAL(KIND = 8), INTENT(IN)                   :: EQ_H_pho(:,:)
        !REAL(KIND = 8),              ALLOCATABLE     :: Hpert_abs2_real(:,:), E_2(:), E_1(:)
        COMPLEX(KIND = 8), INTENT(IN)                :: H_Coup(:,:), EQ_H_CF(:,:)
        COMPLEX(KIND = 8), ALLOCATABLE , INTENT(OUT) :: H_total(:,:), H_total_NC(:,:)
        !COMPLEX(KIND = 8), ALLOCATABLE               :: Hpert_abs2(:,:)

        ! Allocate zero field total Hamiltonian
        ALLOCATE(H_total(coup_dim, coup_dim), &
                 H_total_NC(coup_dim, coup_dim))

        ! Form zero field total Hamiltonian with no coupling Hamiltonian
        ! HCF+HZEE+HPHO
        IF (pho_dim > 1) THEN 
            H_total_NC = KRON_PROD(EQ_H_CF, C_IDENTITY(pho_dim)) + KRON_PROD(IDENTITY(elec_dim),&
                                   EQ_H_pho)
            H_total = H_total_NC + H_Coup
        ELSE
            H_total_NC = EQ_H_CF
            H_total = H_total_NC
        END IF


    END SUBROUTINE CALC_H_EQ_TOTAL


    SUBROUTINE WRITE_ZF_INFO(elec_dim, pho_dim, coup_dim, gJ, CF_vecs, CF_vals, EQ_H_pho, &
                             EQ_H_total, EQ_H_total_NC, basis_labels, B_quant)
        USE matrix_tools
        ! Writes Equilibrium eigenvectors and eigenvalues to output file
        IMPLICIT NONE
        INTEGER, INTENT(IN)                          :: elec_dim, pho_dim, coup_dim, &
                                                        basis_labels(:, :)
        INTEGER                                      :: i, j, row, col, num_bars
        REAL(KIND = 8), INTENT(IN)                   :: CF_vals(:), gJ, EQ_H_pho(:,:), B_quant
        REAL(KIND = 8), ALLOCATABLE                  :: mJ(:), cf_percents(:,:), state_mu_z(:), &
                                                        state_labels(:), &
                                                        tot_vals(:), tot_vals_NC(:), &
                                                        tot_percents_NC(:,:), tot_percents(:,:)
        COMPLEX(KIND = 8), INTENT(IN)                :: CF_vecs(:,:), EQ_H_total(:,:), &
                                                        EQ_H_total_NC(:,:)
        COMPLEX(KIND = 8), ALLOCATABLE               :: tot_vecs(:,:), tot_vecs_NC(:,:)
        CHARACTER(LEN = 10000)                       :: FMT, FMTT, state_labels_char, output_char

        ! Calculate all the information that will be put in the output file

        ! Calculate Magnetic moment (along z) of each state
        CALL CALC_MAGNETIC_MOMENT(elec_dim, gJ, CF_vecs, state_mu_z)

        ! Make vector of mJ values
        ! -mJ to +mJ
        ALLOCATE(mJ(elec_dim))
        mJ = 0.0_8
        DO i = 1,elec_dim
            mJ(i) = -real(elec_dim-1, 8)/2.0_8 + (i-1)*1.0_8
        END DO

        ! Calculate <Jz> values of each state
        ALLOCATE(state_labels(elec_dim))
        state_labels = 0.0_8
        DO i = 1,elec_dim
            DO j = 1,elec_dim
                state_labels(i) = state_labels(i) + mJ(j)*(ABS(REAL(CF_vecs(j,i),8))**2 + ABS(aimag(CF_vecs(j, i)))**2)
            END DO
        END DO

        ! Calculate % composition of crystal field states
        ALLOCATE(cf_percents(elec_dim, elec_dim))
        DO row = 1, elec_dim
            cf_percents(row,:) = ABS(REAL(CF_vecs(:,row),8))**2 + ABS(aimag(CF_vecs(:,row)))**2
        END DO

        ! Allocate array for phonon eigenvalues
        ALLOCATE(PHO_vals(pho_dim))
        PHO_vals = 0.0_8

        ! Get Phonon eigenvalues 
        CALL DIAGONALISE('U',EQ_H_pho, PHO_vals)
        PHO_vals = PHO_vals - PHO_vals(1)

        ! Allocate arrays for total eigenvectors and eigenvalues without coupling
        ALLOCATE(tot_vecs_NC(coup_dim, coup_dim), &
                 tot_vals_NC(coup_dim), &
                 tot_percents_NC(coup_dim, coup_dim))

        tot_vecs_NC     = (0.0_8, 0.0_8)
        tot_vals_NC     = 0.0_8
        tot_percents_NC = 0.0_8

        ! Diagonalise H_total without coupling
        CALL DIAGONALISE('U', EQ_H_total_NC, tot_vecs_NC, tot_vals_NC)
        tot_vals_NC = tot_vals_NC - tot_vals_NC(1)

        ! Percentage composition of total eigenvectors without coupling
        DO row = 1, coup_dim
            tot_percents_NC = ABS(tot_vecs_NC)**2
        END DO

        ! Allocate arrays for total eigenvectors and eigenvalues with coupling
        ALLOCATE(tot_vecs(coup_dim, coup_dim), &
                 tot_vals(coup_dim), &
                 tot_percents(coup_dim, coup_dim))

        tot_vecs     = (0.0_8, 0.0_8)
        tot_vals     = (0.0_8, 0.0_8)
        tot_percents = 0.0_8

        ! Diagonalise H_total with coupling
        CALL DIAGONALISE('U', EQ_H_total, tot_vecs, tot_vals)
        tot_vals = tot_vals - tot_vals(1)

        ! Percentage composition of total eigenvectors with coupling
        DO row = 1, coup_dim
            tot_percents = ABS(tot_vecs)**2
        END DO

        ! Open output file
        OPEN(66, FILE = 'firms_sim.out', STATUS = 'UNKNOWN', POSITION = 'APPEND')

        ! Write equilbrium electronic structure section header
        WRITE(66,'(A)') ''
        CALL SECTION_HEADER('Equilibrium electronic structure', 66)
        WRITE(66,'(A)') ''
        WRITE(66,'(A)') 'A very small magnetic field has been applied to quantise the states'
        WRITE(66,'(A, F15.7, A)') 'B_quant = ', B_quant, ' T'
        WRITE(66,'(A)') ''

        ! Write <J_z> values to output file  
        CALL SUB_SECTION_HEADER('<J_z> Values', 66)
        DO row = 1, elec_dim
            WRITE(66,'(A, I2, A, F15.7)') 'State ', row,' <J_z> = ',state_labels(row)
        END DO
        WRITE(66,*) 

        ! Write magnetic moment values to output file
        CALL SUB_SECTION_HEADER('z-Magnetic Moment (mu_B)', 66)
        DO row = 1, elec_dim
            WRITE(66,'(A, I2, A, F15.7)') 'State ', row,' mu = ',state_mu_z(row)
        END DO
        WRITE(66,*) 

        ! Crystal Field eigenvalues - these have been replaced!
        CALL SUB_SECTION_HEADER('Equilibrium Crystal Field Eigenvalues (cm^-1)', 66)
        DO row = 1, elec_dim
            WRITE(66,'(A, I2, A, F15.7)') 'State ', row,' E = ', CF_vals(row) - CF_vals(1)
        END DO
        WRITE(66,*) 

        ! Crystal Field eigenvectors

        ! % composition
        CALL SUB_SECTION_HEADER('Equilibrium Crystal Field Vectors (percentages)', 66)

        ! Formatting for mJ labels used in % composition table
        WRITE(FMTT,*) '(',('F5.1,A,', i = 1, elec_dim),')'
        WRITE(state_labels_char,TRIM(FMTT)) (mJ(i),'  ',i = 1, elec_dim)
        
        ! Write mJ labels for table
        WRITE(output_char,'(2A)') ' mJ =     ', TRIM(state_labels_char)
        WRITE(66,'(A)') TRIM(output_char)

        ! Formatting for the number of underscores
        num_bars = LEN(TRIM(ADJUSTL(output_char))) + 1
        WRITE(66,'(A)') REPEAT('‾',num_bars)

        ! Formatting for % compositions
        WRITE(FMT, '(A, I0, A)') '(A, I2, ', elec_dim,'F7.2)'

        ! Write % compositions
        DO row = 1, elec_dim
            WRITE(66,FMT) 'State ', row, (100.0_8*cf_percents(row,col),col = 1, elec_dim)
        END DO

        DEALLOCATE(cf_percents)

    ! Write equilbrium Vibrational Modes section header
        WRITE(66,'(A)') ''
        CALL SECTION_HEADER('Equilibrium Vibrational Modes', 66)
        WRITE(66,'(A)') ''

        IF (pho_dim > 1) THEN
            CALL SUB_SECTION_HEADER('Equilibrium Phonon Eigenvalues (cm^-1)', 66)
            DO row = 1, pho_dim
                WRITE(66,'(A, I2, A, F15.7)') 'State ', row,' E = ', PHO_vals(row)
            END DO

        ELSE
            WRITE(66,'(A)') '                          No modes are used for coupling                        '
        END IF

    ! Write equilbrium electronic structure section header
        WRITE(66,'(A)') ''
        CALL SECTION_HEADER('Zero Field Total Hamiltonian', 66)
        WRITE(66,'(A)') ''

        WRITE(66,'(A, I0, A)') 'Total Hamiltonian contains ', coup_dim,' states'
        WRITE(66,'(A)') ''
        WRITE(66,'(A)') 'A very small magnetic field has been applied to quantise the states'
        WRITE(66,'(A, F15.7, A)') 'B_quant = ', B_quant, ' T'
        WRITE(66,'(A)') ''

        ! Total eigenvalues without coupling
        CALL SUB_SECTION_HEADER('Zero Field Eigenvalues of total Hamiltonian without coupling term (cm^-1)', 66)
        DO row = 1, coup_dim
            WRITE(66,'(A, I5, A, F15.7)') 'State ', row,' E = ', tot_vals_NC(row)
        END DO
        WRITE(66,'(A)') ''
    
        ! Total eigenvalues with coupling
        CALL SUB_SECTION_HEADER('Zero Field Eigenvalues of total Hamiltonian with coupling term (cm^-1)', 66)
        DO row = 1, coup_dim
            WRITE(66,'(A, I5, A, F15.7)') 'State ', row,' E = ', tot_vals(row)
        END DO
        WRITE(66,'(A)') ''
    
        ! Total eigenvectors without coupling
        WRITE(66,'(A)') '           Zero Field Total Hamiltonian Vectors without coupling term           '
        CALL SUB_SECTION_HEADER('Percentage contribution of |KD,n_j,...> states', 66)
        WRITE(66,'(A)') '           Each column is an eigenvector           '

        ! Header line of table
        WRITE(FMT,*) '(A,',('A, I0,',j = 1, n_coup_modes), ')'
        WRITE(66,FMT) 'KD',(' n',j, j = 1, n_coup_modes)

        ! Formatting for % compositions
        WRITE(FMT, '(A, I0, A, I0, A)') '(SP, I2, SS,', n_coup_modes, 'I3, A,', coup_dim, 'F7.2)'

        ! Write % compositions
        DO row = 1, coup_dim
            WRITE(66,FMT) (basis_labels(row,col), col = 1, n_coup_modes+1), '  ', (100.0_8*tot_percents_NC(row, col), col = 1, coup_dim)
        END DO
        WRITE(66,'(A)') ''

        ! Total eigenvectors without coupling
        WRITE(66,'(A)') '           Zero Field Total Hamiltonian Vectors with coupling term           '
        CALL SUB_SECTION_HEADER('Percentage contribution of |KD,n_j,...> states', 66)
        WRITE(66,'(A)') '           Each column is an eigenvector           '

        ! Header line of table
        WRITE(FMT,*) '(A,',('A, I0,',j = 1, n_coup_modes), ')'
        WRITE(66,FMT) 'KD',(' n',j, j = 1, n_coup_modes)

        ! Formatting for % compositions
        WRITE(FMT, '(A, I0, A, I0, A)') '(SP, I2, SS,', n_coup_modes, 'I3, A,', coup_dim, 'F7.2)'

        ! Write % compositions
        DO row = 1, coup_dim
            WRITE(66,FMT) (basis_labels(row,col), col = 1, n_coup_modes+1), '  ', (100.0_8*tot_percents(row, col), col = 1, coup_dim)
        END DO
        WRITE(66,'(A)') ''

        ! Close output file
        CLOSE(66)

    END SUBROUTINE WRITE_ZF_INFO

    SUBROUTINE CALC_MAGNETIC_MOMENT(elec_dim, gJ, CF_vecs, state_mu)
        USE matrix_tools
        IMPLICIT NONE
        REAL(KIND = 8), ALLOCATABLE, INTENT(OUT)   :: state_mu(:)
        REAL(KIND = 8), INTENT(IN)                 :: gJ
        COMPLEX(KIND = 8), INTENT(IN)              :: CF_vecs(:,:)
        COMPLEX(KIND = 8), ALLOCATABLE             :: JZ(:,:)
        COMPLEX(KIND = 8), ALLOCATABLE             :: pos(:,:)
        INTEGER, INTENT(IN)                        :: elec_dim
        INTEGER                                    :: i

    ! Calculate JZ operator
        ALLOCATE(JZ(elec_dim, elec_dim))
        JZ = (0.0_8, 0.0_8)
        DO i = 1,elec_dim
            JZ(i,i) = -0.5_8*(elec_dim - 1) + (i-1)*1.0_8
        END DO

    ! Allocate array for magnetic moment values
        ALLOCATE(state_mu(elec_dim), pos(elec_dim, elec_dim))
        state_mu = 0.0_8
        pos      = 0.0_8

    ! Calculate magnetic moment of each state
    ! Use Zeeman Hamiltonian in Z direction and change to eigenbasis of crystal field Hamiltonian
        pos = -0.466866577042538_8*gJ*JZ
        CALL EXPECTATION('F',CF_vecs,pos)
        DO i = 1, elec_dim
            state_mu(i) = -real(pos(i,i),8)
        END DO

    END SUBROUTINE CALC_MAGNETIC_MOMENT

    SUBROUTINE CALC_AV_SPECTRUM(EQ_H_CF, EQ_H_pho, H_Coup, elec_dim, pho_dim, coup_dim, &
                                field_vecs, field_angs, n_field_strengths, n_field_vecs, gJ, &
                                lower_e_bound, upper_e_bound, total_absorbance, e_axis, resolution,&
                                temperature, basis_labels, dip_deriv, trans_treat, elec_scale, &
                                ir_grid_size, CF_vecs, linewidth)
        USE matrix_tools
        USE stev_ops
        USE OMP_LIB
        IMPLICIT NONE
        REAL(KIND = 8), ALLOCATABLE               ::  EQ_H_pho_BB(:,:), tot_vals(:), pops(:), &
                                                      absorbance(:), trans_energies(:), &
                                                      real_vecs(:,:), sel_mat(:,:)
        REAL(KIND = 8)                            ::  e_step, curr_field(3), curr_angs(2)
        REAL(KIND = 8), ALLOCATABLE               ::  EQ_H_pho(:,:)
        REAL(KIND = 8), INTENT(IN)                ::  lower_e_bound, upper_e_bound, &
                                                      field_vecs(:,:,:), temperature, gJ, &
                                                      field_angs(:,:), dip_deriv(:,:), elec_scale,&
                                                      linewidth
        REAL(KIND = 8), INTENT(OUT), ALLOCATABLE  ::  total_absorbance(:,:), e_axis(:)
        COMPLEX(KIND = 8), ALLOCATABLE            ::  EQ_H_CF_BB(:,:), H_ZEE(:,:), H_total(:,:), &
                                                      tot_vecs(:,:), trans_prob(:,:, :)
        COMPLEX(KIND = 8), INTENT(IN)             ::  H_Coup(:,:), EQ_H_CF(:,:), CF_vecs(:,:)
        INTEGER, INTENT(IN)                       ::  elec_dim, pho_dim, coup_dim, &
                                                      n_field_strengths, n_field_vecs, &
                                                      resolution, ir_grid_size
        INTEGER                                   ::  i, fval, fvec, k, num_trans, basis_labels(:,:)
        INTEGER, ALLOCATABLE                      ::  trans_indices(:,:), diff(:)
        CHARACTER(LEN = *)                        ::  trans_treat

        ! Get CF and PHO Hamiltonians in the coupled basis
        ALLOCATE(EQ_H_pho_BB(coup_dim,coup_dim))
        EQ_H_pho_BB   = (0.0_8, 0.0_8)
        EQ_H_pho_BB   = KRON_PROD(IDENTITY(elec_dim), EQ_H_pho)

        DEALLOCATE(EQ_H_PHO)

        ! Create array of energies for absorbances
        ALLOCATE(e_axis(resolution))
        e_step = (upper_e_bound - lower_e_bound)/real(resolution,8)
        e_axis = [(lower_e_bound+e_step*real(i,8), i = 0, resolution - 1)]

        ! Create array of total absorbances and single absorbances
        ALLOCATE(total_absorbance(n_field_strengths, resolution))
        total_absorbance = 0.0_8

        ! Generate selection rules matrix
        ALLOCATE(sel_mat(coup_dim, coup_dim))
        sel_mat = 1.0_8

        ALLOCATE(diff(n_coup_modes + 1))

        DO k = 1, coup_dim
            DO i = 1, coup_dim

                ! Difference in phonon labels
                diff = abs(basis_labels(i,:) - basis_labels(k,:))

                ! Remove non-transitions
                IF (count(basis_labels(i,:) == basis_labels(k,:)) == n_coup_modes+1) THEN
                    !write(6,*) 'identical',basis_labels(i,:), '   ', basis_labels(k,:)
                    sel_mat(i,k) = 0.0_8
                ! Remove transitions which have no change in electronic part
                ELSE IF (basis_labels(i,1) == basis_labels(k,1)) THEN
                    !write(6,*) 'pure vibration',basis_labels(i,:), '   ', basis_labels(k,:)
                    sel_mat(i,k) = 0.0_8
                ! Remove transitions which involve overtones
                ELSE IF (any(diff(2:) > 1)) THEN
                    !write(6,*) 'overtone',basis_labels(i,:), '   ', basis_labels(k,:)
                    sel_mat(i,k) = 0.0_8
                ! Remove transitions with multiple changes to phonon quantum numbers
                ELSE IF (count(basis_labels(i,2:) == basis_labels(k,2:)) < n_coup_modes - 1) THEN
                    !write(6,*) 'multiphonon',basis_labels(i,:), '   ', basis_labels(k,:)
                    sel_mat(i,k) = 0.0_8
                ! Remove electronic transitions - i.e. those with no change in phonon quantum number
                ELSE IF (count(basis_labels(i,2:) == basis_labels(k,2:)) == n_coup_modes) THEN
                    !write(6,*) 'pure electronic',basis_labels(i,:), '   ', basis_labels(k,:)
                    sel_mat(i,k) = 0.0_8
                END IF

            END DO
        END DO

        CALL EXECUTE_COMMAND_LINE('rm -f vibrational_trans.txt')
        CALL EXECUTE_COMMAND_LINE('rm -f electronic_trans.txt')
        CALL EXECUTE_COMMAND_LINE('rm -f vibronic_cold_trans.txt')
        CALL EXECUTE_COMMAND_LINE('rm -f vibronic_hot_trans.txt')

    !$OMP PARALLEL SHARED(e_axis, gJ, field_vecs, EQ_H_CF, H_Coup, temperature), &
    !$OMP& SHARED(n_field_strengths, total_absorbance, n_field_vecs, pho_dim, EQ_H_pho_BB), &
    !$OMP& SHARED(upper_e_bound, lower_e_bound, coup_dim, linewidth), &
    !$OMP& SHARED(trans_treat, elec_scale, ir_grid_size, mask_trans, basis_labels, sel_mat)
        !$OMP DO SCHEDULE(DYNAMIC,1), &
        !$OMP PRIVATE(num_trans, trans_energies, fval, fvec), & 
        !$OMP& PRIVATE(curr_field, tot_vecs, tot_vals, H_total, H_ZEE, EQ_H_CF_BB, pops), &
        !$OMP& PRIVATE(absorbance, trans_indices, trans_prob, curr_angs, real_vecs)
        DO fval = 1, n_field_strengths
            !Write(6,'(A,I0)') 'field number ', fval
            DO fvec = 1, n_field_vecs

                ALLOCATE(EQ_H_CF_BB(coup_dim,coup_dim), H_ZEE(elec_dim, elec_dim), &
                         H_total(coup_dim, coup_dim), tot_vals(coup_dim), &
                         tot_vecs(coup_dim, coup_dim), pops(coup_dim), &
                         absorbance(resolution), real_vecs(coup_dim, coup_dim))
                ALLOCATE(trans_indices(coup_dim**2, 2))
                ALLOCATE(trans_energies(coup_dim**2))
                trans_energies = 0.0_8
                num_trans = 0

                ! Set arrays to zero
                tot_vecs       = (0.0_8,0.0_8)
                tot_vals       = 0.0_8
                H_total        = (0.0_8,0.0_8)
                H_ZEE          = (0.0_8,0.0_8)
                EQ_H_CF_BB     = (0.0_8,0.0_8)
                pops           = 0.0_8
                trans_indices  = 0
                absorbance     = 0.0_8
                real_vecs      = 0.0_8

                ! Calculate zeeman Hamiltonian
                curr_field = field_vecs(fval, fvec, :)
                curr_angs  = field_angs(fvec, :)
                CALL CALC_H_ZEE(curr_field(1), curr_field(2), curr_field(3), gJ, H_ZEE)

                ! and move to "zero" field electronic eigenbasis
                call EXPECTATION('F', CF_vecs, H_ZEE)

                ! Add CF and ZEE Hamiltonians and move to coupled basis
                EQ_H_CF_BB  = KRON_PROD(EQ_H_CF + H_ZEE , C_IDENTITY(pho_dim))

                ! Form total Hamiltonian in coupled basis
                IF (pho_dim > 1) THEN
                    H_total = EQ_H_CF_BB + EQ_H_pho_BB + H_Coup
                ELSE
                    H_total = EQ_H_CF_BB
                END IF

                ! Diagonalise total Hamiltonian in coupled basis
                CALL DIAGONALISE('U', H_total, tot_vecs, tot_vals)
                tot_vals = tot_vals - tot_vals(1)

                ! Get transition probabilities 
                CALL CALC_TRANS_PROB(dip_deriv, elec_dim, pho_dim, n_vib_states, coup_dim,&
                                     curr_angs, trans_prob, ir_grid_size, tot_vecs, trans_treat,&
                                     elec_scale, n_coup_modes, CF_vecs, sel_mat, mask_trans)

                ! Calculate Boltzmann populations of energy levels
                CALL CALC_POPS(tot_vals, pops, temperature)

                ! Calculate transition energies
                CALL CALC_TRANS_ENERGIES(tot_vals, upper_e_bound, lower_e_bound, trans_energies,&
                                         trans_indices, num_trans, pops)

                ! Calculate current spectrum
                CALL CALC_SPECTRUM(trans_energies, trans_indices, num_trans, pops, &
                                   trans_prob, absorbance, e_axis, linewidth, ir_grid_size, tot_vecs,&
                                   n_coup_modes)

                ! Add to total absorbance for this field value
                total_absorbance(fval, :) = total_absorbance(fval, :) + absorbance

                IF (single_axis /= 'no' .AND. fval == 1) CALL WRITE_TRANSITION_INFO(trans_energies, trans_indices, basis_labels, tot_vecs)

                DEALLOCATE(EQ_H_CF_BB, H_ZEE, H_total, tot_vals, tot_vecs, pops, &
                           trans_prob, absorbance, trans_indices, real_vecs, trans_energies)

            END DO
        END DO
        !$OMP END DO NOWAIT 
    !$OMP END PARALLEL 

    total_absorbance = total_absorbance / n_field_vecs
    END SUBROUTINE CALC_AV_SPECTRUM

    SUBROUTINE WRITE_TRANS_ENERGIES(trans_energies, num_trans, n_field_strengths, n_field_vecs)
        IMPLICIT NONE
        INTEGER, INTENT(IN)          :: num_trans(:,:), n_field_strengths, n_field_vecs
        INTEGER                      :: fval, fvec, tnum, fvi
        REAL(KIND = 8), INTENT(IN)   :: trans_energies(:,:,:)
        CHARACTER(LEN = 100)         :: fmt, file_name

        WRITE(fmt, '(A, I0, A)') '(',n_field_vecs, 'F15.7)'

        fvi = 0

        DO fval = 1, n_field_strengths
            WRITE(file_name, '(A, I0, A)') 'trans_energies_',fval,'.dat' 
            OPEN(33, FILE = file_name, STATUS='UNKNOWN')
                fvi = fvi + 1
                DO tnum = 1, num_trans(fval,fvi)
                    WRITE(33,fmt) (trans_energies(fval, fvec, tnum), fvec = 1, n_field_vecs)
                END DO
            CLOSE(33)
        END DO

    END SUBROUTINE WRITE_TRANS_ENERGIES

    RECURSIVE SUBROUTINE CALC_TRANS_PROB(dip_deriv, elec_dim, pho_dim, n_vib_states, coup_dim,&
                                         curr_angs, trans_prob, ir_grid_size, tot_vecs, trans_treat,&
                                         elec_scale, n_coup_modes, CF_vecs, sel_mat, mask_trans)
        USE constants
        IMPLICIT NONE
        COMPLEX(KIND = 8), ALLOCATABLE , INTENT(OUT) :: trans_prob(:,:, :)
        COMPLEX(KIND = 8), INTENT(IN)                :: tot_vecs(:,:), CF_vecs(:,:)
        COMPLEX(KIND = 8), ALLOCATABLE               :: trans_prob_PHO(:,:,:), trans_prob_CF(:,:,:), &
                                                        sel_mat_current(:,:)
        REAL(KIND = 8), INTENT(IN)                   :: dip_deriv(:,:), curr_angs(2), elec_scale, &
                                                        sel_mat(:,:)
        !REAL(KIND = 8)                               :: E_field(3), B_field(3), E_field_r(3), &
        !                                                B_field_r(3)
        REAL(KIND = 8), ALLOCATABLE                  :: qbig(:,:,:), trans_prob_PHOR(:,:,:)!, zeta(:)
        INTEGER                                      :: i, j 
        INTEGER, INTENT(IN)                          :: elec_dim, n_vib_states, pho_dim, &
                                                        coup_dim, ir_grid_size, n_coup_modes
        CHARACTER(LEN = *), INTENT(IN)               :: trans_treat
        LOGICAL, INTENT(IN)                          :: mask_trans

        ALLOCATE(trans_prob_CF(elec_dim, elec_dim, ir_grid_size), &
                 trans_prob_PHO(pho_dim,pho_dim, ir_grid_size), &
                 trans_prob_PHOR(pho_dim,pho_dim, ir_grid_size), &
                 trans_prob(coup_dim, coup_dim, ir_grid_size), &
                 qbig(n_coup_modes, n_vib_states, n_vib_states), &
                 sel_mat_current(coup_dim, coup_dim))
        trans_prob_CF   = (0.0_8, 0.0_8)
        trans_prob_PHO  = (0.0_8, 0.0_8)
        trans_prob_PHOR = (0.0_8, 0.0_8)
        qbig            = (0.0_8, 0.0_8)
        sel_mat_current = (0.0_8, 0.0_8)

        sel_mat_current = sel_mat

        CALL EXPECTATION('F', tot_vecs, sel_mat_current)

        ! ALLOCATE(zeta(ir_grid_size))
        ! zeta = 0.0_8

        ! DO i = 1, ir_grid_size
        !     zeta(i) = 2.0_8*pi/REAL(ir_grid_size,8) * REAL(i,8)
        ! END DO`

        DO i = 1, ir_grid_size


            ! E_field = [0.0_8, sin(zeta(i)), cos(zeta(i))]
            ! B_field = [0.0_8, sin(zeta(i)+pi/2.0_8), cos(zeta(i)+pi/2.0_8)]

            ! E_field_r = ALT_MATMUL(rmat(curr_angs(1),curr_angs(2), 0.0_8),E_field)
            ! B_field_r = ALT_MATMUL(rmat(curr_angs(1),curr_angs(2), 0.0_8),B_field)

            IF (n_coup_modes > 1) THEN
                do j = 1, n_coup_modes
                    qbig(j, :, :) = ANTI_IDENTITY(n_vib_states)
                END DO
                CALL COUPLE_MODE_ARRAY(qbig, trans_prob_PHOR(:,:,i), n_coup_modes, n_vib_states)
                trans_prob_PHO(:,:,i) = trans_prob_PHOR(:,:,i)
            ELSE
                ! IF (TRIM(trans_treat) == 'simple') THEN
                trans_prob_PHO(:,:,i) = ANTI_IDENTITY(n_vib_states)
                ! ELSE
                !     CALL GET_Q_MATRIX(n_vib_states, trans_prob_PHO(:,:,i))
                !     trans_prob_PHO(:,:,i) = trans_prob_PHO(:,:,i) * SUM(E_field_r * dip_deriv(1,:))
                ! END IF
            END IF

            ! Calculate magnetic transition probability from magnetic moment operator
            ! CALL MAG_TRANS(B_field_r, elec_dim - 1, trans_prob_CF(:,:,i), trans_treat, CF_vecs)
            CALL MAG_TRANS(elec_dim - 1, trans_prob_CF(:,:,i), trans_treat, CF_vecs)

            ! Add together magnetic and electric dipole transition moments 
            trans_prob(:,:,i) = KRON_PROD(trans_prob_CF(:,:,i)*elec_scale, IDENTITY(pho_dim)) &
                              + KRON_PROD(IDENTITY(elec_dim), trans_prob_PHO(:,:,i))

            ! Move to eigenbasis of total Hamiltonian
            CALL EXPECTATION('F', tot_vecs, trans_prob(:,:,i))

            !Apply our selection rules to remove pure electronic, pure vibrational, and overtone
            !signals
            if (mask_trans) then
                trans_prob(:,:,i) = trans_prob(:,:,i) * sel_mat_current
            end if

            trans_prob(:,:,i) = ABS(trans_prob(:,:,i))**2

        END DO
    END SUBROUTINE CALC_TRANS_PROB

    ! RECURSIVE FUNCTION rmat(alpha, beta, gamma) RESULT(mat)
    !     USE matrix_tools
    !     IMPLICIT NONE
    !     REAL(KIND = 8), INTENT(IN) :: alpha, beta, gamma
    !     REAL(KIND = 8)             :: mat(3,3), R1(3,3), R2(3,3), R3(3,3)

    !     ! Rz
    !     R1(1,1) = cos(alpha)
    !     R1(1,2) = sin(alpha)
    !     R1(1,3) = 0.0_8
    !     R1(2,1) = -sin(alpha)
    !     R1(2,2) = cos(alpha)
    !     R1(2,3) = 0.0_8
    !     R1(3,1) = 0.0_8
    !     R1(3,2) = 0.0_8
    !     R1(3,3) = 1.0_8

    !     ! Ry'
    !     R2(1,1) = cos(beta)
    !     R2(1,2) = 0.0_8
    !     R2(1,3) = -sin(beta)
    !     R2(2,1) = 0.0_8
    !     R2(2,2) = 1.0_8
    !     R2(2,3) = 0.0_8
    !     R2(3,1) = sin(beta)
    !     R2(3,2) = 0.0_8
    !     R2(3,3) = cos(beta)

    !     ! Rz''
    !     R3(1,1) = cos(gamma)
    !     R3(1,2) = sin(gamma)
    !     R3(1,3) = 0.0_8
    !     R3(2,1) = -sin(gamma)
    !     R3(2,2) = cos(gamma)
    !     R3(2,3) = 0.0_8
    !     R3(3,1) = 0.0_8
    !     R3(3,2) = 0.0_8
    !     R3(3,3) = 1.0_8

    !     mat = (ALT_MATMUL(ALT_MATMUL(R3, R2), R2))

    ! END FUNCTION

    !RECURSIVE SUBROUTINE MAG_TRANS(B, two_J, trans, trans_treat, CF_vecs)
    RECURSIVE SUBROUTINE MAG_TRANS(two_J, trans, trans_treat, CF_vecs)
        use constants, ONLY : muB
        IMPLICIT NONE
        ! REAL(KIND = 8), INTENT(IN)     :: B(3)
        INTEGER, INTENT(IN)            :: two_J
        INTEGER                        :: row
        COMPLEX(KIND = 8)              :: ii
        COMPLEX(KIND = 8), INTENT(OUT) :: trans(:,:)
        COMPLEX(KIND = 8), INTENT(IN)  :: CF_vecs(:,:)
        COMPLEX(KIND = 8), ALLOCATABLE :: JZ(:,:), JP(:,:), JM(:,:)
        CHARACTER(LEN=*), INTENT(IN)   :: trans_treat

        ii = (0.0_8, 1.0_8)

        CALL CALC_JZPM(two_J, JZ, JP, JM)

        ! IF (TRIM(trans_treat) == 'simple') THEN

            DO row = 1, two_J
                trans(row, row + 1) = 1.0_8
            END DO 

            trans = trans + TRANSPOSE(trans)

            DO row = 1, two_J + 1
                trans(row, row) = 1.0_8
            END DO

        ! ELSE

        !     trans = 4.0_8/3.0_8 * muB*(0.5_8*(JP+JM)*B(1)&
        !           + (1.0_8/(2.0_8*ii))*(JP-JM)*B(2) &
        !           + JZ*B(3))

        ! END IF

        call EXPECTATION('F', CF_vecs, trans)

    END SUBROUTINE MAG_TRANS

    RECURSIVE SUBROUTINE CALC_H_ZEE(Bx, By, Bz, gJ, H_ZEE)
        use constants
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)      :: Bx, By, Bz, gJ
        COMPLEX(KIND = 8), ALLOCATABLE  :: JP(:,:), JM(:,:), JZ(:,:)
        COMPLEX(KIND = 8), INTENT(OUT)  :: H_ZEE(:,:)
        COMPLEX(KIND = 8)               :: ii
        !INTEGER :: ROW, COL

        ii = (0.0_8, 1.0_8)

        CALL CALC_JZPM(elec_dim - 1, JZ, JP, JM)

        H_ZEE = muB*gJ*(0.5_8*(JP+JM)*Bx + (1.0_8/(2.0_8*ii))*(JP-JM)*By + JZ*Bz)

        ! OPEN(70, FILE = 'zeeman.txt', status = 'UNKNOWN')

        ! DO row = 1, SIZE(JZ,1)
        !     WRITE(70, '(8F15.7)') (REAL(JZ(row, col),8), col = 1, 8)
        ! END DO

        ! CLOSE(70)

        DEALLOCATE(JZ, JP, JM)

    END SUBROUTINE CALC_H_ZEE

    RECURSIVE SUBROUTINE CALC_JZPM(two_J, JZ, JP, JM)
        IMPLICIT NONE
        INTEGER, INTENT(IN)                         :: two_J
        INTEGER                                     :: i, k
        REAL(KIND = 8)                              :: mJ
        COMPLEX(KIND = 8), ALLOCATABLE, INTENT(OUT) :: JZ(:,:), JP(:,:), JM(:,:)

        ALLOCATE(JZ(two_J + 1, two_J + 1), &
                 JP(two_J + 1, two_J + 1), &
                 JM(two_J + 1, two_J + 1))

        JZ = (0.0_8, 0.0_8)
        JP = (0.0_8, 0.0_8)
        JM = (0.0_8, 0.0_8)
        
        DO i = 1,two_J + 1
            JZ(i,i) = -0.5_8*REAL(two_J, 8) + REAL(i-1, 8)*1.0_8
            DO k = 1, two_J + 1
                mJ = -0.5_8*REAL(two_J, 8) + REAL(k-1, 8)*1.0_8
                IF (i == k+1) JP(i,k) = SQRT(0.5_8*REAL(two_J, 8)&
                                      * (0.5_8*REAL(two_J, 8)+1)-mJ*(mJ+1.0_8))
                IF (i == k-1) JM(i,k) = SQRT(0.5_8*REAL(two_J, 8)&
                                      * (0.5_8*REAL(two_J, 8)+1)-mJ*(mJ-1.0_8))
            END DO
        END DO

    END SUBROUTINE CALC_JZPM


    RECURSIVE SUBROUTINE CALC_POPS(energies, pops, temperature)
        USE constants
        REAL(KIND = 8), INTENT(IN)    :: energies(:), temperature
        REAL(KIND = 8), INTENT(OUT)   :: pops(:)
        REAL(KIND = 8)                :: Z
        INTEGER                       :: j

        ! Set partition function and pops to zero
        Z    = 0.0_8
        pops = 0.0_8
    
        ! Calculate patition function
        DO j = 1,SIZE(energies) 
            Z = Z + exp(-energies(j)/(kB * temperature))
        END DO
    
        ! Calculate population
        DO j = 1,SIZE(energies) 
            pops(j) = exp(-energies(j)/(kB * temperature))/Z
        END DO
    
    END SUBROUTINE CALC_POPS

    RECURSIVE SUBROUTINE CALC_TRANS_ENERGIES(energies, upper_e_bound, lower_e_bound, &
                                             trans_energies, trans_indices, num_trans, pops)
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN)    :: energies(:), upper_e_bound, lower_e_bound, pops(:)
        REAL(KIND = 8), INTENT(OUT)   :: trans_energies(:)
        REAL(KIND = 8)                :: diff
        INTEGER, INTENT(OUT)          :: trans_indices(:,:)
        INTEGER, INTENT(OUT)          :: num_trans
        INTEGER                       :: i, f

        num_trans = 0
        DO i = 1, SIZE(energies)
            DO f = 1, SIZE(energies)
                diff = energies(f) - energies(i)
                IF (diff > 0.0_8 .AND. diff > lower_e_bound .AND. diff < upper_e_bound .AND. pops(i) > 0.001_8) THEN
                    num_trans = num_trans + 1
                    trans_energies(num_trans) = diff
                    trans_indices(num_trans,1) = i
                    trans_indices(num_trans,2) = f
                END IF
            END DO
        END DO

    END SUBROUTINE  CALC_TRANS_ENERGIES

    RECURSIVE SUBROUTINE CALC_SPECTRUM(trans_energies, trans_indices, num_trans, pops, &
                                       trans_prob, absorbance, e_axis, width, ir_grid_size, tot_vecs, &
                                       n_coup_modes)
        IMPLICIT NONE
        COMPLEX(KIND = 8), INTENT(IN) :: trans_prob(:,:,:), tot_vecs(:,:)
        REAL(KIND = 8), INTENT(IN)    :: trans_energies(:), pops(:), e_axis(:), width
        REAL(KIND = 8), INTENT(OUT)   :: absorbance(:)
        REAL(KIND = 8), ALLOCATABLE   :: real_vecs(:,:)
        INTEGER, INTENT(IN)           :: trans_indices(:,:), num_trans, ir_grid_size, &
                                         n_coup_modes
        INTEGER                       :: j, k, i
        INTEGER, ALLOCATABLE          :: diff(:)

        ALLOCATE(real_vecs(size(tot_vecs,1),size(tot_vecs,1)))
        real_vecs = abs(tot_vecs)**2

        ALLOCATE(diff(n_coup_modes + 1))

        ! Loop over IR grid
        DO i = 1, ir_grid_size

            ! Loop over entire energy range
            DO k = 1, num_trans
                DO j = 1, SIZE(e_axis)

                    absorbance(j) = absorbance(j) + GAUSSIAN(pops(trans_indices(k,1))*REAL(trans_prob(trans_indices(k,1),trans_indices(k,2),i), 8), &
                                               trans_energies(k), width, e_axis(j) &
                                              )
                END DO
            END DO

        END DO

        absorbance = absorbance/ir_grid_size

    END SUBROUTINE CALC_SPECTRUM


    SUBROUTINE WRITE_TRANSITION_INFO(trans_energies, trans_indices, basis_labels, tot_vecs)
        IMPLICIT NONE
        INTEGER, INTENT(IN)           :: trans_indices(:,:), basis_labels(:,:)
        INTEGER                       :: j
        REAL(KIND = 8), INTENT(IN)    :: trans_energies(:)
        COMPLEX(KIND = 8), INTENT(IN) :: tot_vecs(:,:)
        CHARACTER(LEN = 500)          :: FMT, header


        ! Make header 
        WRITE(header, '(A)')  'Transitions at first field strength'

        OPEN(66, FILE='firms_sim.out', STATUS='UNKNOWN', POSITION='APPEND')

            CALL SUB_SECTION_HEADER(header, 66)

            WRITE(66,'(A)') 'States listed have the largest contribution to the eigenvectors of Htotal'
            WRITE(66,*) 

            WRITE(FMT,*) '(A,',('A, I0,',j = 1, n_coup_modes), 'A,',('A, I0,',j = 1, n_coup_modes),')'
            WRITE(66,FMT) '      ENERGY          :  KD',(' n',j, j = 1, n_coup_modes), ' -->  KD',(' n',j, j = 1, n_coup_modes)

            ! Make format identifier for energies and state labels
            WRITE(FMT, '(A, I0, A, I0, A)') '(F15.6, A, SP, I3, SS, ', SIZE(basis_labels,2)-1, 'I3, A, SP, I3, SS, ', SIZE(basis_labels,2)-1, 'I3)'

            DO j = 1, SIZE(trans_indices)

                if (trans_indices(j,1) == 0) exit

                WRITE(66,FMT) trans_energies(j), ' cm^-1 : ', basis_labels(MAXLOC(ABS(tot_vecs(:,trans_indices(j,1)))**2),:), ' --> ', basis_labels(MAXLOC(ABS(tot_vecs(:,trans_indices(j,2)))**2),:)

            END DO

        CLOSE(66)


    END SUBROUTINE WRITE_TRANSITION_INFO

    SUBROUTINE SAVE_OUTPUT_DATA(field_strengths, total_absorbance, e_axis, field_lower, &
                                field_upper, field_step, mode_numbers, n_coup_modes)
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN) :: field_strengths(:),  total_absorbance(:,:), e_axis(:), &
                                      field_lower, field_upper, field_step
        INTEGER, INTENT(IN)        :: mode_numbers(:), n_coup_modes
        INTEGER                    :: row, col, i
        CHARACTER(LEN = 100)       :: FMT
        CHARACTER(LEN = 200)       :: filename


        ! Delete old data file
        IF (n_coup_modes == 1) THEN
            WRITE(filename,'(A, I0, A)') 'data_',mode_numbers(1),'.dat'
            CALL EXECUTE_COMMAND_LINE('rm -f data.dat '//TRIM(filename))
        ELSE
            WRITE(FMT, *) '(A, I0',(', A, I0',i = 2, n_coup_modes),', A)'
            ! Need to go backwards here because we flip the modes at the start!
            WRITE(filename,FMT) 'data_',mode_numbers(n_coup_modes),('_',mode_numbers(i),i = n_coup_modes - 1, 1, -1),'.dat'
            CALL EXECUTE_COMMAND_LINE('rm -f data.dat '//TRIM(filename))
        END IF

    ! Open output file
        OPEN(66, FILE = TRIM(filename), STATUS = 'UNKNOWN')

            WRITE(FMT, '(A, I0, A)') '(',SIZE(mode_numbers),'I5)'

            WRITE(66,'(3F15.7)') field_lower, field_upper, field_step
            WRITE(66,FMT) mode_numbers
            WRITE(66,*) lower_e_bound, upper_e_bound, resolution

            WRITE(FMT, '(A, I0, A)') '(',SIZE(e_axis),'F30.15)'

            DO row = 1, SIZE(e_axis)
                WRITE(66, TRIM(FMT)) (total_absorbance(col, row), col = 1, SIZE(field_strengths))
            END DO

        CLOSE(66)

    END SUBROUTINE SAVE_OUTPUT_DATA

    FUNCTION GAUSSIAN(A, B, C, X) RESULT(curve)
        ! Calculates the value of the gaussian with height A, position B, and width C
        ! at position X
        IMPLICIT NONE
        REAL(KIND = 8), INTENT(IN) :: A, B, C, X
        REAL(KIND = 8)             :: curve

        curve = EXP(-(X-B)**2/(2*C**2)) *  A

    END FUNCTION GAUSSIAN

    SUBROUTINE READ_PHONON_CF_POLYNOMIAL(polynomials_file, cf_parameter_array, num_modes)
        ! Reads in CFP_polynomials.dat
        IMPLICIT NONE
        INTEGER, INTENT(IN)                       :: num_modes
        INTEGER                                   :: q, IDUM, j, col, reason
        REAL(KIND = 8), ALLOCATABLE, INTENT(OUT)  :: cf_parameter_array(:,:,:,:)
        CHARACTER(LEN=*), INTENT(IN)              :: polynomials_file

    ! Read polynomial fits of delta CFPs i.e. the equilibrium CFPs have been removed
    !  OEFs are not included (so these are the "real" CFPs)
    !  The cf_parameter_array is laid out as
    !  (num_modes, num_ranks, num_orders, num_coefficients)
    !  Where num_modes is the number of vibrational modes
    !  num_ranks and num_orders are the k and q values of the crystal field (1,1) = B_2^-2; (2,3) = B_4^-2 etc.
    !    n.b. that elements which contain non-existant parameters such as cf_parameter_array(1, 1, 12) are zero
    !  num_coefficients is the number of coefficients in the 3rd order polynomial fit where (1) = a of ax^3 etc

    ! The array is laid out as follows
        ALLOCATE(cf_parameter_array(num_modes,3, 13, 3))
        cf_parameter_array = 0.0_8
        
    ! Open file
        OPEN(33,FILE = TRIM(polynomials_file), STATUS = 'OLD')
        ! Loop over modes and read in coefficients for each parameter
            DO j = 1,num_modes
                READ(33, *, IOSTAT=reason) 
        
            ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL PRINT_ERROR('!!!!                   Error reading CFP_polynomials.dat                    !!!!','')
                    ELSE IF (reason < 0) THEN
                        CALL PRINT_ERROR('!!!!           Error: Reached end of CFP_polynomials.dat too soon           !!!!','')
                    END IF  
            ! k = 2 q = -2 -> 2
                DO q = 1, 5                
                    READ(33,*, IOSTAT = reason) IDUM, IDUM, (cf_parameter_array(j,1,q,col), col=1, 3)

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL PRINT_ERROR('!!!!                   Error reading CFP_polynomials.dat                    !!!!','')
                    ELSE IF (reason < 0) THEN
                        CALL PRINT_ERROR('!!!!           Error: Reached end of CFP_polynomials.dat too soon           !!!!','')
                    END IF  
                END DO

            ! k = 4 q = -4 -> 4
                DO q = 1, 9
                    READ(33,*, IOSTAT = reason) IDUM, IDUM, (cf_parameter_array(j,2,q,col), col=1, 3)

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL PRINT_ERROR('!!!!                   Error reading CFP_polynomials.dat                    !!!!','')
                    ELSE IF (reason < 0) THEN
                        CALL PRINT_ERROR('!!!!           Error: Reached end of CFP_polynomials.dat too soon           !!!!','')
                    END IF  
                END DO

            ! k = 6 q = -6 -> 6
                DO q = 1, 13
                    READ(33,*, IOSTAT = reason) IDUM, IDUM, (cf_parameter_array(j,3,q,col), col=1, 3)

                ! Check file is not corrupted or too short
                    IF (reason > 0)  THEN
                        CALL PRINT_ERROR('!!!!                   Error reading CFP_polynomials.dat                    !!!!','')
                    ELSE IF (reason < 0) THEN
                        CALL PRINT_ERROR('!!!!           Error: Reached end of CFP_polynomials.dat too soon           !!!!','')
                    END IF  
                END DO  

            END DO
    ! Close CFP_polynomials.dat
        CLOSE(33)

    END SUBROUTINE READ_PHONON_CF_POLYNOMIAL

    SUBROUTINE GET_Q_MATRIX(n_vib_states, pho_mat)
        USE matrix_tools
        IMPLICIT NONE
        INTEGER, INTENT(IN)            :: n_vib_states
        INTEGER                        :: n, m
        COMPLEX(KIND = 8), INTENT(OUT) :: pho_mat(:,:)

        pho_mat = (0.0_8, 0.0_8)

        DO n = 0, n_vib_states - 1
            DO m = 0, n_vib_states - 1
                IF (n == m - 1) THEN
                    pho_mat(n + 1, m + 1) = SQRT(REAL(m,8)/2.0_8)
                ELSE IF (n == m + 1) THEN
                    pho_mat(n + 1, m + 1) = SQRT((REAL(m,8)+1.0_8)/2.0_8)
                END IF
            END DO
        END DO

    END SUBROUTINE GET_Q_MATRIX

    SUBROUTINE PRINT_ERROR(message, message_2)
        ! Print date and time followed by error message
        ! then abort
        IMPLICIT NONE
        CHARACTER(LEN = 10)            :: DATE, TIME
        CHARACTER(LEN = *), INTENT(IN) :: message, message_2

    ! Open output file
        OPEN(66, FILE = 'firms_sim.out', STATUS = 'UNKNOWN', POSITION = 'APPEND')

    ! Get date and time
        CALL DATE_AND_TIME(DATE = DATE,TIME = TIME)

    ! Write error message(s) to output file
        WRITE(66,'(A)') TRIM(message)
        IF (message_2 /= '') WRITE(66,'(A)') TRIM(message_2)

    ! Write abort time and date to output file
        WRITE(66,'(13A)')'-----------          firms_sim aborts at ',TIME(1:2),':',TIME(3:4),':',TIME(5:6),' on ',DATE(5:6),'-',DATE(7:8),'-',DATE(1:4),'         -----------'

    ! Close output file and stop program
        CLOSE(66)
        STOP 1

    END SUBROUTINE PRINT_ERROR

SUBROUTINE SECTION_HEADER(title, unit)
    ! Writes a section header to unit
    ! Three lines
    ! ---------
    ! - title -
    ! ---------
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: title
    INTEGER, INTENT(IN)            :: unit
    INTEGER                        :: num_l_spaces, num_r_spaces

    num_l_spaces = (70 - LEN_TRIM(title)) / 2
    num_r_spaces = (70 - LEN_TRIM(title)) / 2

    IF (MODULO(LEN_TRIM(title),2) /= 0) THEN
        num_l_spaces = num_l_spaces + 1

    END IF

    WRITE(unit,'(A)')   REPEAT('-',80)
    WRITE(unit, '(5A)') REPEAT('-',5), REPEAT(' ',num_l_spaces), TRIM(title), &
                        REPEAT(' ',num_r_spaces), REPEAT('-',5)
    WRITE(unit,'(A)')   REPEAT('-',80)

END SUBROUTINE SECTION_HEADER

SUBROUTINE SUB_SECTION_HEADER(title, unit)
    ! Writes a subsection section header to unit
    ! Two lines
    !   title
    !   ‾‾‾‾‾
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: title
    INTEGER, INTENT(IN)            :: unit

    INTEGER                        :: num_l_spaces, num_r_spaces

    num_l_spaces = (80 - LEN_TRIM(title)) / 2
    num_r_spaces = (80 - LEN_TRIM(title)) / 2

    IF (MODULO(LEN_TRIM(title),2) /= 0) THEN
        num_l_spaces = num_l_spaces + 1

    END IF

    WRITE(unit, '(3A)') REPEAT(' ',num_l_spaces), &
                        TRIM(title), &
                        REPEAT(' ',num_r_spaces)

    WRITE(unit,'(3A)')  REPEAT(' ',num_l_spaces), &
                        REPEAT('‾',LEN_TRIM(title)), &
                        REPEAT(' ',num_r_spaces)

END SUBROUTINE SUB_SECTION_HEADER

SUBROUTINE WARNING(title, unit)
    ! Writes a warning header to unit
    ! Three lines
    ! *********
    ! * title *
    ! *********
    IMPLICIT NONE
    CHARACTER(LEN = *), INTENT(IN) :: title
    INTEGER, INTENT(IN)            :: unit

    INTEGER                        :: num_l_spaces, num_r_spaces

    num_l_spaces = (70 - LEN_TRIM(title)) / 2
    num_r_spaces = (70 - LEN_TRIM(title)) / 2

    IF (MODULO(LEN_TRIM(title),2) /= 0) THEN
        num_l_spaces = num_l_spaces + 1

    END IF

    WRITE(unit,'(A)')   REPEAT('*',80)
    WRITE(unit, '(5A)') REPEAT('*',5), REPEAT(' ',num_l_spaces), TRIM(title), &
                        REPEAT(' ',num_r_spaces), REPEAT('*',5)
    WRITE(unit,'(A)')   REPEAT('*',80)

END SUBROUTINE WARNING


END PROGRAM firms_sim
