#! /usr/bin/env python3

from collections import defaultdict
import argparse
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cm
from matplotlib.ticker import AutoMinorLocator, MultipleLocator, LinearLocator
import argparse
import numpy.ma as ma
from numpy import linalg as LA
import os
import sys
from sympy.physics.wigner import wigner_3j, wigner_6j
from scipy.special import factorial

def read_in_command_line():
    # Read in command line arguments

    parser = argparse.ArgumentParser(description='Calculates coupling strength S for each vibrational mode')
    parser.add_argument('polyfile', type=str, metavar='<polynomials_file>',
                        help='File containing polynomial fit of CFPs without OEFs - from pt_fit')
    parser.add_argument('mode_energies_file', type=str, metavar='<mode_energies_file>',
                        help='File containing vibrational mode energies')
    parser.add_argument('ion', type=str, metavar='<ion_symbol>',
                        help='Chemical symbol of metal ion')
    parser.add_argument('ox_state', type=int, metavar='<ox_state>',
                        help='Oxidation state of metal ion')
    parser.add_argument('--irreps', type=str, metavar='<file_name>',
                        help='Color codes strength plot based on mode symmetries listed in file')
    parser.add_argument('--save', type=str, metavar='<png/svg>',
                        help='Save figure with given format e.g. --save png or svg')
    parser.add_argument('--xlow', type=int, metavar='<number>',
                        help='Lower limit of x axis')
    parser.add_argument('--xup', type=int, metavar='<number>',
                        help='Upper limit of x axis')
    parser.add_argument('--figsize', nargs=2, type=int, metavar='<width,height>', default = (5,5),
                        help='Figure size (w,h)')
    user_args = parser.parse_args()

    # Check format is png or svg
    if user_args.save:

        save_format = user_args.save.lower()

        # If format not png or svg then exit with error
        if save_format not in ['png','svg','.png','.svg', 'pdf', '.pdf']:
            parser.error(' Save format is unsupported ')

        # Remove dot from file extension if user adds it
        if save_format[0] == '.':
            user_args.save = save_format[1:]

    return user_args


def kq_to_num(k, q):
	index = k + q 
	for kn in range(1, int(k/2)):
		index += 2*(k-2*kn) + 1
	return index

def stevens_to_wybourne(CFPs, k_max):
    """Transforms Crystal Field parameters from Wybourne notation to Stevens notation

    Parameters:
        CFPs (np.array)  : CFPs in Stevens notation
        k_max (integer)  : maximum value of k

    Returns:
        w_CFPs (np.array) : CFPs in Wybourne notation
    """

    n_CFPs = {2 : 5, 4 : 14, 6 : 27}

    lmbda = [
             np.sqrt(6.)/3.,
             -np.sqrt(6.)/6.,
             2.,
             -np.sqrt(6.)/6.,
             np.sqrt(6.)/3.,
             4.*np.sqrt(70.)/35.,
             -2.*np.sqrt(35.)/35.,
             2.*np.sqrt(10.)/5.,
             -2*np.sqrt(5.)/5.,
             8.,
             -2.*np.sqrt(5.)/5.,
             2.*np.sqrt(10.)/5.,
             -2.*np.sqrt(35.)/35.,
             4.*np.sqrt(70.)/35.,
             16.*np.sqrt(231.)/231.,
             -8.*np.sqrt(77.)/231.,
             8.*np.sqrt(14.)/21.,
             -8.*np.sqrt(105.)/105.,
             16.*np.sqrt(105.)/105.,
             -4.*np.sqrt(42.)/21.,
             16.,
             -4.*np.sqrt(42.)/21.,
             16.*np.sqrt(105.)/105.,
             -8.*np.sqrt(105.)/105.,
             8.*np.sqrt(14.)/21.,
             -8.*np.sqrt(77.)/231.,
             16.*np.sqrt(231.)/231.
            ]

    w_CFPs = np.zeros(n_CFPs[k_max], dtype = "complex_")

    for k in range(2, k_max + 2, 2):
        for q in range(-k, k + 1):
            if q == 0:
                w_CFPs[kq_to_num(k,q)] = lmbda[kq_to_num(k,q)]*CFPs[kq_to_num(k,q)]
            elif q > 0:
                w_CFPs[kq_to_num(k,q)] = lmbda[kq_to_num(k,q)]*(CFPs[kq_to_num(k,q)] + 1j*CFPs[kq_to_num(k,-q)])
            elif q < 0:
                w_CFPs[kq_to_num(k,q)] = lmbda[kq_to_num(k,q)]*(-1)**q*(CFPs[kq_to_num(k,-q)] - 1j*CFPs[kq_to_num(k,q)])

    return w_CFPs

def calc_oef(n, J, L, S):
	"""Calculates operator equivalent factors using the 
		approach of https://arxiv.org/pdf/0803.4358.pdf

	Parameters:
		n (int) : number of electrons in f shell
		J (int) : J Quantum number
		L (int) : L Quantum number
		S (int) : S Quantum number

	Returns:
		oefs (np.array) : array of operator equivalent factors for each parameter k,q
	"""


	def oef_lambda(p, J, L, S):
		return (-1)**(J+L+S+p)*(2*J+1) * wigner_6j(J, J, p, L, L, S)/wigner_3j(p, L, L, 0, L, -L)

	def oef_K(p, k, n):
		K = 7. * wigner_3j(p, 3, 3, 0, 0, 0)
		if n <= 7:
			n_stop = n
		else:
			n_stop = n-7
			if k == 0:
				K -= np.sqrt(7)

		Kay = 0
		for j in range(1, n_stop+1):
			Kay += (-1.)**j * wigner_3j(k,3,3,0,4-j,j-4)

		return K*Kay

	def oef_RedJ(J, p):
		return 1./(2.**p) * (factorial(2*J+p+1)/factorial(2*J-p))**0.5

	# Calculate OEFs and store in array 
	# Each parameter Bkq has its own parameter
	oef = np.zeros(27)
	shift = 0
	for k in range(2, np.min([6, int(2*J)])+2, 2):
		oef[shift:shift+2*k+1] = float(oef_lambda(k,J,L,S) * oef_K(k,k,n)/oef_RedJ(J,k))
		shift += 2*k+1

	return oef

def get_oef(ion, ox_state):

	# Calculate number of f electrons
	if ion.lower() == 'ce': num_f_elec =  5
	if ion.lower() == 'pr': num_f_elec =  6
	if ion.lower() == 'nd': num_f_elec =  7
	if ion.lower() == 'sm': num_f_elec =  8
	if ion.lower() == 'eu': num_f_elec =  9
	if ion.lower() == 'gd': num_f_elec = 10
	if ion.lower() == 'tb': num_f_elec = 11
	if ion.lower() == 'dy': num_f_elec = 12
	if ion.lower() == 'ho': num_f_elec = 13
	if ion.lower() == 'er': num_f_elec = 14
	if ion.lower() == 'tm': num_f_elec = 15
	if ion.lower() == 'yb': num_f_elec = 16

	num_f_elec = num_f_elec - ox_state

	# Calculate S
	if num_f_elec == 7 or num_f_elec == 14:
		print('No orbital moment! ')
		exit()
	elif num_f_elec < 7:
		S = num_f_elec / 2.
	elif num_f_elec > 7:
		S = (14.0 - num_f_elec) / 2.

	# Calculate L
	if num_f_elec in [1,6,8,13]:
		L = 3.
	elif num_f_elec in [2,5,9,12]:
		L = 5.
	elif num_f_elec in [3,4,10,11]:
		L = 6.

	# Calculate J
	if num_f_elec < 7:
		J = ABS(L - S)
	elif num_f_elec > 7:
		J = L + S

	return calc_oef(num_f_elec, J, L, S)

def calc_strength(params):
	#Calculates strength parameters for a set of Stevens parameters

	#Convert Stevens parameters to Wybourne scheme
	wparams = abs(stevens_to_wybourne(params, 6)) # abs takes the norm of the imaginary number
	sum_k2 = 0.
	sum_k4 = 0.
	sum_k6 = 0.

	#Sum over values of q in each rank 

	#Calculate strength within rank (S^k)
	s_k2 = np.sqrt(np.sum(wparams[:5]**2) / 5.)
	s_k4 = np.sqrt(np.sum(wparams[5:14]**2) / 9.)
	s_k6 = np.sqrt(np.sum(wparams[14:]**2) / 13.)
	Sk = [s_k2, s_k4, s_k6]

	#Calculate overall strength as weighted sum of S^k values
	S = np.sqrt(1./3.*(s_k2**2 + s_k4**2 + s_k6**2))

	# Sq
	Sq6 = np.sqrt(1./13. * wparams[26]**2 )
	Sq5 = np.sqrt(1./13. * wparams[25]**2 )
	Sq4 = np.sqrt(1./13. * wparams[24]**2 + 1./9. * wparams[13]**2 )
	Sq3 = np.sqrt(1./13. * wparams[23]**2 + 1./9. * wparams[12]**2 )
	Sq2 = np.sqrt(1./13. * wparams[22]**2 + 1./9. * wparams[11]**2 + 1./5. * wparams[4]**2 )
	Sq1 = np.sqrt(1./13. * wparams[21]**2 + 1./9. * wparams[10]**2 + 1./5. * wparams[3]**2 )
	Sq0 = np.sqrt(1./13. * wparams[20]**2 + 1./9. * wparams[9]**2  + 1./5. * wparams[2]**2 )

	Sq = [Sq6, Sq5, Sq4, Sq3, Sq2, Sq1, Sq0]

	return np.asarray(S), np.asarray(Sq), np.asarray(Sk)

def mode_strengths(CFPs, num_modes):
	#Calculate strength parameters for an array of sets of CFPs

	#Make empty arrays for parameters
	S   = np.zeros(num_modes)
	Sk   = np.zeros([num_modes,3])
	Sq   = np.zeros([num_modes,7])

	for mode in np.arange(num_modes):
		
		#Zero-variables
		S_temp, Sk_temp, Sq_temp = 0., 0., 0.
	
		#Read in 27 parameters
		params = CFPs[mode, :]
	
		#Calculate strength parameters for a given distortion and set of parameters
		S_temp, Sq_temp, Sk_temp = calc_strength(params)
	
		#Add individual parameters to arrays of all parameters
		S[mode] = S_temp
		Sq[mode,:] = Sq_temp
		Sk[mode,:] = Sk_temp

	return S, Sq, Sk

def plot_lum(ax, data, xlow, xup):

	lax = ax.twinx()

	lax.plot(data[:,0], data[:,1], lw=2, color='green')

	#lax.set_xticks(np.arange(0,1200,100))
	lax.set_yticklabels([])

	lax.set_xlabel(r'$\Delta$ Energy (cm$^{-1}$)')
	lax.set_ylabel('Luminescence intensity (arb. units)')
	lax.set_ylim([xlow, xup])

		# Remove plot box lines
	lax.spines['right'].set_visible(False)
	lax.spines['top'].set_visible(False)

	return

def plot_strength(ax, modes, S, irreps, xlow, xup):

	if irreps:
		# Get list of unique symmetries
		unique_irreps = list(set(irreps))

		# Make dictionary of mode number and irrep
		irrep_mode = defaultdict(list)
		for it, irrep in enumerate(irreps):
			irrep_mode[irrep].append(it)
	else:
		unique_irreps = ['A']
		irrep_mode = defaultdict(list)
		irrep_mode['A'] = [it for it in range(np.size(modes))]


	for it, irrep in enumerate(unique_irreps):
		ax.stem(modes[irrep_mode[irrep]], S[irrep_mode[irrep]], linefmt='C{:d}-'.format(it), markerfmt='C{:d}o'.format(it),  basefmt=' ', label=irrep)

	if irreps:
		ax.legend(frameon=False)

	ax.set_ylabel(r'$S$ (cm$^{-1}$)')
	ax.set_xlabel('Mode energy (cm$^{-1}$)')

	ax.set_ylim([0., np.max(S)*1.05])
	if xlow and xup: ax.set_xlim([xlow, xup])

	return

def load_data(user_args):

	# Load mode energies
	try:
		modes     = np.loadtxt(user_args.mode_energies_file)
	except ValueError:
		modes     = np.loadtxt(user_args.mode_energies_file, skiprows=1)

	if np.size(np.shape(modes)) > 1:
		modes = modes[:,0]

	num_modes = np.size(modes)

	# Read in polynomials
	polynomials = np.zeros([num_modes, 27, 3])
	with open(user_args.polyfile,'r') as f:
			for it in range(num_modes):
				line = next(f)
				for param_it in range(27):
					line = next(f)
					polynomials[it, param_it, :] = line.split()[2:]

	# Take linear term of polynomials
	order_one = polynomials[:,:,-1]

	# Get OEF values
	OEFs = get_oef(user_args.ion, user_args.ox_state)

	# Add back in OEFs to polynomial values
	order_one *= OEFs

	# Calculate strength values of each mode
	S, Sq, Sk = mode_strengths(order_one, num_modes)

	return modes, S


if __name__ == '__main__' :

	# Read command line arguments
	user_args = read_in_command_line()

	# Read data and calculate S values
	modes, S = load_data(user_args)

	# Read in symmetry labels if given
	if user_args.irreps:
		irreps = list(np.loadtxt(user_args.irreps, dtype=str))
	else:
		irreps = None

	# Make figure
	fig, ax = plt.subplots(1,1, sharex=True, figsize=user_args.figsize)

	# Add data
	plot_strength(ax, modes, S, irreps, user_args.xlow, user_args.xup)

	# Save S values to file
	np.savetxt('S_j_values.dat', S)

	# Adjust figure
	ax.spines['right'].set_visible(False)
	ax.spines['top'].set_visible(False)
	fig.tight_layout()

	# Save and show
	if user_args.save:
		fig.savefig('Sj_vs_e.{}'.format(user_args.save))

	plt.show()

