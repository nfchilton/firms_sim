\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{hyperref}
\usepackage{fancyvrb}
\usepackage[numbers,super]{natbib}

\title{\texttt{firms\_sim}}
\author{
  Jon Kragskow\\
  \texttt{jon.kragskow@manchester.ac.uk}
  \and
  Nicholas Chilton\\
  \texttt{nicholas.chilton@manchester.ac.uk}
}
\date{}
  
\begin{document}
  
\maketitle
  
\newpage
\tableofcontents

\newpage
\section{Introduction}
   
\texttt{firms\_sim} is a code used to simulate the FIRMS (Far InfraRed MagnetoSpectroscopy) maps of lanthanide complexes. The theory used in \texttt{firms\_sim} is outlined in \citet{Marbey2021} and is briefly summarised here.

\section{Theory}

The total Hamiltonian employed in \texttt{firms\_sim} is defined as

\begin{align}\label{fullH}
	\hat{H}_{\text{T}} &= \hat{H}_{\text{CF}} + \hat{H}_{\text{Zee}} + \sum_j \left[\hat{H}_{\text{vib},j} + \hat{H}_{\text{coup},j} \right]\\
	\nonumber
	&= \sum_{k=2,4,6}\sum_{q=-k}^k B_k^q \hat{O}_k^q + \mu_B g_J \vec{B} \cdot \vec{\hat{J}} + \sum_{j=1}^{3N-6} \left [ \hbar\omega_j \left(n_j + \frac{1}{2} \right) + \hat{H}_{\text{coup},j} \right]
\end{align}

\noindent The first and second terms are the electronic CF and Zeeman Hamiltonians, evaluated in the $|m_J\rangle$ basis of the ground spin-orbit multiplet of the lanthanide in question, the third term is the quantum harmonic oscillator Hamiltonian, evaluated in the basis of vibrational quanta $|n_j\rangle$ for mode $j$, and the fourth term $\hat{H}_{\text{coup},j}$ is the vibronic coupling Hamiltonian; $\mu_B$ is the Bohr magneton, $g_J$ is the Lande g-factor, $\vec{B}$ is the magnetic field vector, $\vec{\hat{B}}$ is the electronic total angular momentum vector operator, $B_k^q$ are the Stevens CF parameters (CFPs), $\hat{O}_k^q$ are the Stevens operators,\cite{Stevens1952} $\hbar\omega_j$ is the energy of vibrational mode $j$, $\hbar$ is the reduced Planck constant, and $N$ is the number of atoms. \\

\noindent To construct the $\hat{H}_{\text{coup},j}$, the CFPs are expanded in a Taylor series in the displacement $Q_j$ along normal mode $j$ centered at the equilibrium displacement $Q_{\text{eq}}=0$.
		
\begin{equation}
	B_k^q (Q_j) = B_k^q(Q_{\text{eq}}) + \sum_j^{3N-6}Q_j \left( \frac{\partial B_k^q}{\partial Q_j} \right)_{\text{eq}} + \sum_j^{3N-6} \sum_{j'}^{3N-6} Q_j Q_{j'} \left( \frac{\partial^2 B_k^q}{\partial Q_j \partial Q_{j'}} \right)_{\text{eq}} + \dots
\end{equation}

\noindent In \texttt{firms\_sim} a first-order approximation is employed in which the linear term is assumed to be dominant. \textit{Ab initio} calculations by \citet{Marbey2021} show this to be the case. This assumption leads to the vibronic coupling Hamiltonian 

\begin{equation}
	\hat{H}_{\text{coup},j} = Q_j \sum_{k=2,4,6}\sum_{q=-k}^k \left( \frac{\partial B_k^q}{\partial Q_j} \right)_{\text{eq}} \hat{O}_k^q	
\end{equation}

\noindent The overall vibronic coupling strength $S_j$ for each mode is defined as

\begin{equation}
S_j = \sqrt{\frac{1}{3}\sum_{k=2,4,6} \frac{1}{2k+1} \sum_{q=-k}^k  \left|\left( \frac{\partial B_q^k}{\partial Q_j} \right)_{\text{eq}} \right|^2}
\end{equation}

\noindent Where $B_q^k$ is a CFP in the Wybourne Scheme.\cite{Mulak2000}\\

\noindent Equation (\ref{fullH}) operates within the weak-coupling limit where vibrational modes are unaffected by the electronic states. Therefore, any matrix element of $\hat{H}_{\text{coup},j}$ for a single mode $j$ in the direct product basis is

\begin{equation}\label{coupel}
	\left\langle m_J',n_j \pm 1 \left|\hat{H}_{\text{coup},j}	\right| m_J,n_j\right\rangle = 	\left\langle m_J',n_j \pm 1 \left|Q_j \sum_{k=2,4,6}\sum_{q=-k}^k \left( \frac{\partial B_k^q}{\partial Q_j} \right)_{\text{eq}} \hat{O}_k^q	\right| m_J,n_j\right\rangle
\end{equation}

The electronic and nuclear parts of this expression are calculated as 
\begin{equation}
	\hat{H}_{\text{coup-e},j} = \left\langle m_J' \left|\sum_{k=2,4,6}\sum_{q=-k}^k \left( \frac{\partial B_k^q}{\partial Q_j} \right)_{\text{eq}} \hat{O}_k^q	\right| m_J\right\rangle
\end{equation}
and 
\begin{equation}
	\hat{H}_{\text{coup-v},j} = \left\langle n_j \pm 1 \left|Q_j 	\right| n_j\right\rangle
\end{equation}
respectively. These two matrices can be used to obtain the matrix representation of $\hat{H}_{\text{coup},j}$ through the Kronecker product. 

\begin{equation}\label{coupeln}
	\hat{H}_{\text{coup},j} = \hat{H}_{\text{coup-e},j} \otimes \hat{H}_{\text{coup-v},j}
\end{equation}

Where the total Hamiltonian can then be written as
\begin{align*}
	\hat{H}_{\text{T}} = &\left(\hat{H}_{\text{CF}} + \hat{H}_{\text{Zee}}\right) \otimes \mathbb{I}_{\text{vib,1}} \otimes \mathbb{I}_{\text{vib,2}} \otimes \dots + \mathbb{I}_{\text{elec}} \otimes \hat{H}_{\text{vib},1}\\ &\otimes \mathbb{I}_{\text{vib,2}} \otimes \dots + \hat{H}_{\text{coup-e},1} \otimes \hat{H}_{\text{coup-v},1} \otimes \mathbb{I}_{\text{vib},2}\\ &\otimes \dots + \hat{H}_{\text{coup-e},2} \otimes \mathbb{I}_{\text{2},1} \otimes \hat{H}_{\text{coup-v},2} \otimes \dots + \dots
\end{align*}

\noindent In practice, $\hat{H}_{\text{Zee}}$ and $\hat{H}_{\text{coup-v},j}$ into the eigenbasis of $\hat{H}_{\text{CF}}$.\\

\noindent The $\left( \frac{\partial B_k^q}{\partial Q_j} \right)_{\text{e  q}}$ for each mode must be provided to \texttt{firms\_sim} in the form of 3rd order polynomials fitted to the changes in the $B_k^q$ from their equilibrium values as a function of displacement (see Appendix A). The displacement along a given mode should be in units of zero point displacement, defined as

\begin{equation}
	Q_j = \sqrt{\frac{\hbar}{\omega_j\mu_j}}
\end{equation}

\noindent The non-zero matrix elements of Equation (\ref{coupeln}) are evaluated as 

\begin{align}
	\left \langle n_j - 1\left | Q_j \right| n_j
	 \right \rangle &= \frac{1}{\sqrt{2}} \left
	  \langle n_j - 1\left | \hat{a} \right| n_j
	   \right \rangle = \frac{n_j}{2} \\
	\left \langle n_j + 1\left | Q_j \right| n_j \right \rangle &= \frac{1}{\sqrt{2}} \left \langle n_j - 1\left | \hat{a}^\dagger \right| n_j \right \rangle = \frac{n_j+1}{2}
\end{align}

\noindent The intensity, $I_{if}$, of a transition between two states $i$ and $f$ is proportional to the product of the square of the transition matrix element  $\langle i | d_{\text{T}}' | f \rangle$ and the Boltzmann population of the initial state $p_i$, which in \texttt{firms\_sim} is convoluted with an empirical Gaussian linewidth function $g(\Delta E_{fi})$ with a standard deviation of 2 cm\textsuperscript{-1}. The matrix representation of the total transition moment in the direct product basis as the sum of the purely electronic ($d_{\text{e}}$) and purely vibrational ($d_{\text{v}}$) moments

\begin{equation}
	d_{\text{T}} = 	d_{\text{e}} \otimes \mathbb{I}_{\text{vib}} +  \mathbb{I}_{\text{vib}} \otimes d_{\text{v}}
\end{equation}

\noindent The vibrational moment is defined according to the vibrational selection rule 

\begin{equation}
	\langle n_j' |d_{\text{v}}|n_j\rangle = \begin{cases}
	1 \text{      if      } n_j' = n_j \pm 1\\
	0 \text{      else}
	\end{cases}
\end{equation}

\noindent Similarly, for the electronic moment

\begin{equation}
	\langle n_j' |d_{\text{e}}|n_j\rangle = \begin{cases}
		1 \text{      if      } m_J' = m_J \pm 1 \text{      or      } m_J' = m_J\\
		0 \text{      else}
	\end{cases}
\end{equation}

\noindent Where the value of the non-zero matrix elements can be scaled by the user.\\

\noindent In \texttt{firms\_sim}, purely electronic and purely vibrational transitions are excluded from the calculation of the FIRMS map. This is required because the approximate transition intensities allow spectral features corresponding to slight vibronic mixing into pure vibrational bands to become dominant. To perform this exclusion, a mask $M$ is defined in the direct product basis as
\begin{equation}
	\langle m_J',n_1',n_2',\dots|M|m_J,n_1,n_2,\dots\rangle = 
	\begin{cases}
		1 \text{      if      } n_j' = n_j\pm 1, m_J' = m_J\pm 1,\\ \text{    and    } n_k' = n_k \text{    for all    } k\neq j\\
		0 \text{    else    }
	\end{cases}
\end{equation}

\noindent Both the matrix representations of $M$ and $d_\text{T}$ are transformed into the eigenbasis of $\hat{H}_\text{T}$, and subsequently the former is applied to the latter by way of the element-wise (Hadamard) product, where $U$ is the matrix of coefficients that diagonalise $\hat{H}_\text{T}$

\begin{equation}
	d_\text{T}'= U^{-1}d_\text{T}U \circ U^{-1}MU
\end{equation}

\noindent As FIRMS experiments are performed on powder samples, an integration over many orientations of the applied magnetic field must be performed. In \texttt{firms\_sim} this is done using the Zaremba-Conroy-Wolfsberg scheme with a user-configurable integration level.\cite{Eden1998}

\newpage
\section{Program operation}
An overview of the operational steps in \texttt{firms\_sim} is as follows:

\begin{itemize}
	\item{Read user input file and set configuration variables}
	\item{Read input data (CFPs and mode energies)}
	\item{Generate vectors for integration over magnetic field orientations}
	\item{Calculate zero field $H_\text{CF}$ and $H_\text{vib}$ and diagonalise}
	\item{Optionally correct CF energies from user input}
	\item{Calculate $H_\text{coup}$}
	\item{Calculate and diagonalise zero field $H_\text{T}$ with and without $H_\text{coup}$}
	\item{Write zero-field information to file}
	\item{Calculate mask matrix $M$}
	\item{Enter main spectrum calculation (loop over field strengths):
	\begin{itemize}
		\item{ Loop over field vectors
				\begin{itemize}
				\item{Calculate $\hat{H}_\text{Zee}$ in eigenbasis of $\hat{H}_\text{CF}$ and move to direct product basis}
				\item{Form $\hat{H}_\text{vib}$ in direct product basis}
				\item{Form $\hat{H}_\text{T}$ and diagonalise}
				\item{Calculate $d_\text{T}'$ and populations of $\hat{H}_\text{T}$ states} 
				\item{Calculate transition energies between states of $\hat{H}_\text{T}$}
				\item{Calculate spectrum as sum of Gaussians at each transition energy, weighted by $d_\text{T}'$ and $p_i$}
			\end{itemize}
			\item{Sum spectra for each field orientation into single spectrum}
		}
		\end{itemize}	
	\item{Print spectrum to file}
}
\end{itemize}



\newpage
\section{Setup}
       
\noindent The most up-to-date installation instructions can be found in the \texttt{firms\_sim} Readme on the GitLab repository landing page \href{https://gitlab.com/nfchilton/firms_sim}{landing page}.

\subsection{Requirements}

\begin{itemize}
\item{Fortran compiler (\texttt{gfortran} (tested 9.3.0) or \texttt{ifort} (tested 19.1.2.258))}
\item{Linear algebra Fortran libraries (lapack+blas (tested 3.9.0-1build1) or MKL (tested 19.1.2.258))}
\item{Python3 (tested 3.8.5 and 3.9.0) with packages listed in \texttt{src/requirements.txt}}
\item{\texttt{Make} (tested 4.2.1)}
\end{itemize}

\subsection{Installation}

\noindent Currently \texttt{firms\_sim} is only tested and officially supported on Linux and macOS, and has been tested on Ubuntu 20.04 running both natively and within the Windows Subsystem for Linux, and on macOS Catalina 10.15.6.\\

\noindent To install \texttt{firms\_sim}, either download or clone a copy of the repository
\begin{Verbatim}
	git clone https://gitlab.com/nfchilton/firms_sim
\end{Verbatim}
then navigate to \texttt{src}
\begin{Verbatim}
	cd src
\end{Verbatim}
and use the makefile
\begin{Verbatim}
	make all install
\end{Verbatim}

\noindent This installs \texttt{firms\_sim} and its ancillary programs to \texttt{/usr/local/bin}.\\

\noindent On Linux the \texttt{Makefile} will default to using \texttt{ifort}, to override this and use \texttt{gfortran} add the optional argument \texttt{compiler=gfortran} to the \texttt{make all install} command. Whereas for macOS, the default compiler is \texttt{gfortran}, which can be overriden using \texttt{compiler=ifort}.\\

\noindent Compilation and installation takes around a minute or so on modern personal computers.

\subsection{Updating}

To update the code simply type navigate to the repository and then use
\begin{Verbatim}
	git pull
\end{Verbatim}
then move to src and run make
\begin{Verbatim}
	cd src;make all install
\end{Verbatim}
If there is an error about local changes, first type
\begin{Verbatim}
	git stash
\end{Verbatim}
and repeat the above steps.

\newpage
\section{Usage Instructions}

\subsection{Required files}

The following files are required by \texttt{firms\_sim} and should all be located in the same directory.

\begin{itemize}

	\item{\texttt{input.dat}: Contains configuration options for simulation}
	\item{\texttt{CFP\_polynomials.dat} : Parameters for 3rd order polynomial fits of change in each CFP as a function of displacement along each mode (see Appendix 1 for layout)}
	\item{\texttt{EQ\_CFPs.dat} : Equilbrium CFPs (see Appendix 1 for layout)}
	\item{\texttt{vib\_energies.dat} : Vibrational mode energies in cm\textsuperscript{-1} (see Appendix 1 for layout)}
	\item{\texttt{eq\_cf\_energies.dat} : OPTIONAL : Alternative equilibrium CF energies in cm\textsuperscript{-1} (see Appendix 1 for layout)}

\end{itemize}

The first four of these files can have any name.
\newpage
\subsection{Input file}

\noindent The input file for \texttt{firms\_sim} consists of mandatory arguments and optional arguments. Arguments can be specified in any order and are not case-sensitive, while lines beginning with \#, !, or * are ignored. The mandatory arguments are given as follows

\begin{Verbatim}[samepage=true]
eq cfps file <file name> 
polynomials file <file name>
mode energies file <file name>
ion <symbol> <ox state number>
modes <number of modes in molecule> 
use modes <mode numbers to use in coupling>
vib states <number of vibrational states per mode>
field <low in Tesla> <high> <step>
energy <low in Tesla> <high> <number of steps>
temperature <number in K>
\end{Verbatim}

While the optional arguments are

\begin{Verbatim}[samepage=true]
zcw <number specifying zcw level>
single axis LETTER <x, y, or z> ! Apply magnetic field only along the specified axis
mask transitions ! Apply transition masking technique
nooef ! Assumes OEFs subsumed into EQ CFPs
replace cf energies ! Replace CF energies with those from file
scale electronic <number> ! Scale electronic transition intensities by number
scale coupling <number> ! Scale all coupling matrix elements by number
fwhm <number> ! FWHM of Gaussian peaks used in simulation of FIRMS spectrum
transition approach <simple/complex>
\end{Verbatim}

\noindent to generate a blank input file containing this information use the command

\begin{Verbatim}
	firms_sim -input
\end{Verbatim}

\subsection{Running a simulation}

\noindent The four files listed in Section 3.1 must be present in the current directory. To run \texttt{firms\_sim} use the command

\begin{Verbatim}
	firms_sim <input_file_name>
\end{Verbatim}

where \texttt{input\_file\_name} is the name of the input file.\\

\noindent By default \texttt{firms\_sim} runs on a single thread, this can be modified by setting the environment variable \texttt{OMP\_NUM\_THREADS=n} where \texttt{n} is the desired number of threads.\\

\subsection{Output files}

\noindent The output of \texttt{firms\_sim} is appended to \texttt{firms\_sim.out} in the current directory and has the general layout

\begin{Verbatim}[samepage=true]
	Input File
	    - Repeated from input.dat
	Metal Ion Data 
	    - oxidation state,
	    - L, S, J quantum numbers
	Magnetic Field Information 
	    - Strength values
	    - Number of field orientations
	Equilibrium electronic structure
	    - Energies and magnetic moments of states
	    - CF Eigenvectors
	Equilibrium Vibrational Modes
	    - Equilibrium energies
	Zero Field Total Hamiltonian
	    - Number of states
	    - Energies with and without coupling Hamiltonian
	    - Eigenvectors with and without coupling Hamiltonian
\end{Verbatim}

\noindent The simulated spectrum is written to \texttt{data\_M1\_M2\_...\_Mn.dat} where \texttt{Mn} are the indices of the coupled vibrational modes. The format of \texttt{data\_M1\_M2\_...\_Mn.dat} is as follows

\begin{Verbatim}[samepage=true]
Min. field, Max. field, Field step
Coupled mode indices
Min. infrared energy, Max infrared energy, Number of energy points
Field_1_energy_point_1, Field_2_energy_point_1, ..., Field_n_energy_point_1
Field_1_energy_point_2, Field_2_energy_point_2, ..., Field_n_energy_point_2
..., ..., ..., ...
Field_1_energy_point_m, Field_2_energy_point_m, ..., Field_n_energy_point_m
\end{Verbatim}

\noindent where \texttt{Field\_1\_energy\_point\_1} is the value of absorbance at the first energy point for the first magnetic field strength.

\subsection{Analysis tools}

The python scripts \texttt{firms\_heatmap.py} and \texttt{firms\_strength.py} are provided in the firms\_sim repository. The former uses a \texttt{data\_M1\_M2\_...\_Mn.dat} file to produce heatmaps similar to those seen in \citet{Marbey2021}. The latter uses the \texttt{CFP\_polynomials.dat} and \texttt{vib\_energies.dat} files to plot the coupling strength parameter $S_j$ for all modes, again giving plots similar to those seen in \citet{Marbey2021}. To see the full options available in these scripts run the following commands 

\begin{Verbatim}
	firms_heatmap -h
	firms_strength -h
\end{Verbatim}

As is explained in \citet{Marbey2021}, owing to the theoretical nature of the spectra calculated in \texttt{firms\_sim}, applying a normalisation scheme consisting of division by the average of all spectra to the FIRMS data would result in artefacts due to division by zero errors. Therefore \texttt{firms\_heatmap} linearly shifts each spectrum by an arbitrary value (default=5 units) such that division of a former "zero intensity" region by an average "zero intensity" region instead returns a value of unity. This parameter has no physical meaning, and serves only to avoid division-by-zero errors.

\newpage
\section{Example}

Example input files are provided in the \texttt{example} directory of the repository. These correspond to a two mode simulation of the FIRMS map of Yb(trensal) using modes 4 and 5 as detailed in \citet{Marbey2021}. Running \texttt{firms\_sim} on these input files should take around a second or so on a single core, and the expected output files are given in \texttt{example/output}.

\appendix
\section{Data file formats}

\subsection{CFP polynomials file}

This file contains fits of the change in each Stevens crystal field parameter (CFP) from its equilibrium value as a function of displacement along a given vibrational mode to a third order polynomial. The required format of this file is as follows

\begin{Verbatim}[samepage=true]
COMMENT LINE THEN DATA FOR MODE 1 = FITTING PARAMETERS TO' a*x3 + b*x2 + c*x 
2 -2    a   b   c
2 -1    a   b   c
...  ...    a   b   c
2  2    a   b   c
4 -4    a   b   c
4 -3    a   b   c
4  ...    a   b   c
4  3    a   b   c
4  4    a   b   c
6 -6    a   b   c
6 ...    a   b   c
6  6    a   b   c
COMMENT LINE THEN DATA FOR MODE 2 = FITTING PARAMETERS TO' a*x3 + b*x2 + c*x 
...
COMMENT LINE THEN DATA FOR MODE ... = FITTING PARAMETERS TO' a*x3 + b*x2 + c*x 
\end{Verbatim}
\noindent an example can be seen in the \texttt{examples} directory. The fits of the change in the CFPs must not include the operator equivalent factors (OEFs).

\newpage
\subsection{Equilibrium CFP file}

This file contains the equilibrium CFPs in Stevens notation. The required format of this file is as follows

\begin{Verbatim}[samepage=true]
parameter for k=2 q=-2
parameter for k=2 q=-1
...
parameter for k=2 q=2
parameter for k=4 q=-4
parameter for k=4 q=-3
...
parameter for k=4 q=3
parameter for k=4 q=4
parameter for k=6 q=-6
...
parameter for k=6 q=6
\end{Verbatim}
\noindent an example can be seen in the \texttt{examples} directory. These values may include the OEFs as long as the correct flag is specified in the \texttt{firms\_sim} input file.

\subsection{Vibrational mode energies file}

This file contains all of the vibrational mode energies in units of cm\textsuperscript{-1} written in a single column with no headers. An example can be seen in the \texttt{examples} directory.

\subsection{Equilibrium CF energies file}

This file contains a new set of the equilibrium crystal field energies in units of cm\textsuperscript{-1} written in a single column with no headers. An example can be seen in the \texttt{examples} directory.

\bibliographystyle{rsc}
\bibliography{bib}



\end{document}