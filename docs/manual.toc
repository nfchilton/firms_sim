\contentsline {section}{\numberline {1}Introduction}{3}{section.1}%
\contentsline {section}{\numberline {2}Theory}{3}{section.2}%
\contentsline {section}{\numberline {3}Program operation}{6}{section.3}%
\contentsline {section}{\numberline {4}Setup}{7}{section.4}%
\contentsline {subsection}{\numberline {4.1}Requirements}{7}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Installation}{7}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Updating}{7}{subsection.4.3}%
\contentsline {section}{\numberline {5}Usage Instructions}{9}{section.5}%
\contentsline {subsection}{\numberline {5.1}Required files}{9}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Input file}{10}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Running a simulation}{10}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}Output files}{11}{subsection.5.4}%
\contentsline {subsection}{\numberline {5.5}Analysis tools}{11}{subsection.5.5}%
\contentsline {section}{\numberline {6}Example}{13}{section.6}%
\contentsline {section}{\numberline {A}Data file formats}{13}{appendix.A}%
\contentsline {subsection}{\numberline {A.1}CFP polynomials file}{13}{subsection.A.1}%
\contentsline {subsection}{\numberline {A.2}Equilibrium CFP file}{14}{subsection.A.2}%
\contentsline {subsection}{\numberline {A.3}Vibrational mode energies file}{14}{subsection.A.3}%
\contentsline {subsection}{\numberline {A.4}Equilibrium CF energies file}{14}{subsection.A.4}%
