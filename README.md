# FIRMS_sim

## Description

This repository contains the firms_sim code and its associated scripts and programs used to simulate far-infrared magnetospectroscopy spectra.

## Repository Structure

The firms_sim source code and its ancillary programs is located in `src/`. Libraries and Fortran modules are located in `libs`. 

`src/` contains a `Makefile` which can be used to compile and install all the programs and scripts. 

## Requirements

* Fortran compiler (`gfortran` or `ifort`)
* Linear algebra Fortran libraries (lapack+blas or MKL)
* Python3 with packages listed in `src/requirements.txt`
* Make

## Installation

Currently firms_sim is only tested and officially supported on Linux and Mac.

To install firms_sim locally, navigate to `src` and `make all install`. The firms_sim programs will be installed to `/usr/local/bin`.

On Linux the installation will default to using `ifort`, to override this and use `gfortran` add the optional argument `compiler=gfortran` to the `make all install` command. Whereas for mac, the default compiler is `gfortran`, which can be overriden using `compiler=ifort`.

### Manchester - CSF Instructions

If you are planning on installing firms_sim on the University of Manchester's Computational Shared Facility there are a few extra steps needed to get access to the required compilers. This only needs to be done once, and if you have already installed other Chilton Group programs you may not need to do all of these steps!

Navigate to your home folder

```
cd
```

Open the file `.bashrc` in `nano`

```
nano .bashrc
```

and add the following lines to the bottom of the file

```
module load apps/git/2.19.0
module load apps/binapps/anaconda3/2019.07
module load compilers/intel/18.0.3
module load tools/env/proxy
```

If these lines are already present then you do *not* need to add them again.

To save and exit `nano` press CTRL+o, press Enter (or return), and then press CTRL+x.

Then `source` this file with

```
source .bashrc
```
If you do not already have a `git` folder in your csf home, make one

```
mkdir git
```

The same goes for a `bin` folder

```
mkdir bin
```

Navigate to your `git` folder 

```
cd git
```

and clone this repository - this gives you your own version which you can use and modify if you need to.

```
git clone https://gitlab.com/nfchilton/firms_sim
```

Then navigate to `src`

```
cd src
```

and install

```
make all install
```

You should now be able to use all of the firms_sim tools on the csf, try firms_sim using

``` 
firms_sim -h
```

Which should print a help section

## Updating

To update the code simply type navigate to the repository and then use

```
git pull
```
then move to `src` and run `make`
```
cd src;make all install
```

If you get an error about local changes, first type
```
git stash
```
and repeat the above steps

## Usage

For instructions on how to use firms_sim and its ancillary programs, consult the manual in the `docs` directory, or on the Chilton Group Dropbox.

## Bugs

Please report all bugs through Gitlab using the issues tab.
